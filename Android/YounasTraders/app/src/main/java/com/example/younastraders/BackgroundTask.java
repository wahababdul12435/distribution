package com.example.younastraders;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundTask extends AsyncTask<String,Void,String>{

    private CallBack callBack;
    public ListView listView;

    public CallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    private String response = "";
//    private ProgressDialog dialog;
    Context ctx;

    BackgroundTask(Context ctx)
    {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
//        dialog = new ProgressDialog(ctx);
//        dialog.setMessage("Progress start");
//        dialog.show();
    }

    protected String doInBackground(String... params) {
        String sendInvoice = "http://10.0.2.2/GitKraken/distribution/PHP/YounasTraders/SendInvoice.php";
        String sendRegistration = "http://10.0.2.2/GitKraken/distribution/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=register";
//        String subareaData = "http://10.0.2.2/GitKraken/distribution/PHP/YounasTraders/AndroidDataGet/DatabaseSync.php?datafield=Subarea";

        String tableData = params[0];
        if(tableData.equals("order"))
        {
            String dealerId = params[1];
            String productId = params[2];
            String quantity = params[3];
            String unit = params[4];
            String orderPrice = params[5];
            String bonus = params[6];
            String discount = params[7];
            String finalPrice = params[8];
            String latitude = params[9];
            String longitude = params[10];
            String order_place_area = params[11];
            String date = params[12];
            String time = params[13];
            String salesmanId = params[14];
            String status = params[15];
            try {
                URL objurl = new URL(sendInvoice);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerId, "UTF-8")+"&"+
                        URLEncoder.encode("product_id", "UTF-8")+"="+URLEncoder.encode(productId, "UTF-8")+"&"+
                        URLEncoder.encode("quantity", "UTF-8")+"="+URLEncoder.encode(quantity, "UTF-8")+"&"+
                        URLEncoder.encode("unit", "UTF-8")+"="+URLEncoder.encode(unit, "UTF-8")+"&"+
                        URLEncoder.encode("orderprice", "UTF-8")+"="+URLEncoder.encode(orderPrice, "UTF-8")+"&"+
                        URLEncoder.encode("bonus", "UTF-8")+"="+URLEncoder.encode(bonus, "UTF-8")+"&"+
                        URLEncoder.encode("discount", "UTF-8")+"="+URLEncoder.encode(discount, "UTF-8")+"&"+
                        URLEncoder.encode("finalprice", "UTF-8")+"="+URLEncoder.encode(finalPrice, "UTF-8")+"&"+
                        URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                        URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                        URLEncoder.encode("order_place_area", "UTF-8")+"="+URLEncoder.encode(order_place_area, "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8")+"="+URLEncoder.encode(date, "UTF-8")+"&"+
                        URLEncoder.encode("time", "UTF-8")+"="+URLEncoder.encode(time, "UTF-8")+"&"+
                        URLEncoder.encode("salesman_id", "UTF-8")+"="+URLEncoder.encode(salesmanId, "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8")+"="+URLEncoder.encode(status, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                InputStream objis = objhttpurlconnection.getInputStream();
                objis.close();
                objhttpurlconnection.disconnect();

                response = "Order Sent Successfully";

                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            response = "Order Not Sent Successfully";
        }
        else if(tableData.equals("register"))
        {
            String userName = params[1];
            String userID = params[2];
            String password = params[3];
            String userPower = params[4];
            String regDate = params[5];
            String status = params[6];
            try {
                URL objurl = new URL(sendRegistration);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("user_name", "UTF-8") +"="+URLEncoder.encode(userName, "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userID, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(password, "UTF-8")+"&"+
                        URLEncoder.encode("user_power", "UTF-8")+"="+URLEncoder.encode(userPower, "UTF-8")+"&"+
                        URLEncoder.encode("reg_date", "UTF-8")+"="+URLEncoder.encode(regDate, "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8")+"="+URLEncoder.encode(status, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                InputStream objis = objhttpurlconnection.getInputStream();
                objis.close();
                objhttpurlconnection.disconnect();

                response = "Registration Request Sent Successfully";

                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            response = "Registration Request Not Sent";
        }

        return response;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
//        dialog.show();
    }

    @Override
    protected void onPostExecute(String value) {
//        dialog.dismiss();
        if(callBack != null)
        {
            callBack.onResult(value);
        }
    }

    public interface CallBack{
        void onResult(String value);
    }
}
