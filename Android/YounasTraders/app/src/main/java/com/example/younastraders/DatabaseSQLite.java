package com.example.younastraders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.io.File;

public class DatabaseSQLite extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "YounasTraders.db";
    public static final String TABLE_NAME = "order_info";
    public static final String COL_1 = "id";
    public static final String COL_2 = "dealer_id";
    public static final String COL_3 = "product_id";
    public static final String COL_4 = "quantity";
    public static final String COL_5 = "unit";
    public static final String COL_6 = "order_price";
    public static final String COL_7 = "bonus";
    public static final String COL_8 = "discount";
    public static final String COL_9 = "final_price";
    public static final String COL_10 = "latitude";
    public static final String COL_11 = "longitude";
    public static final String COL_12 = "order_place_area";
    public static final String COL_13 = "date";
    public static final String COL_14 = "time";
    public static final String COL_15 = "salesman_id";
    public static final String COL_16 = "status";

    public static final String DEALER_INFO_TABLE = "dealer_info";
    public static final String DEALER_COL_1 = "dealer_id";
    public static final String DEALER_COL_2 = "dealer_area_id";
    public static final String DEALER_COL_3 = "dealer_name";
    public static final String DEALER_COL_4 = "dealer_contact";
    public static final String DEALER_COL_5 = "dealer_address";
    public static final String DEALER_COL_6 = "dealer_type";
    public static final String DEALER_COL_7 = "dealer_cnic";
    public static final String DEALER_COL_8 = "dealer_lic_num";
    public static final String DEALER_COL_9 = "dealer_lic_exp";
    public static final String DEALER_COL_10 = "dealer_status";

    public static final String SUBAREA_INFO_TABLE = "subarea_info";
    public static final String SUBAREA_COL_1 = "subarea_id";
    public static final String SUBAREA_COL_2 = "area_id";
    public static final String SUBAREA_COL_3 = "sub_area_name";
    public static final String SUBAREA_COL_4 = "subarea_status";

    public static final String PRODUCT_INFO_TABLE = "product_info";
    public static final String PRODUCT_COL_1 = "product_id";
    public static final String PRODUCT_COL_2 = "product_name";
    public static final String PRODUCT_COL_3 = "final_price";
    public static final String PRODUCT_COL_4 = "product_status";

    public static final String USER_ACCOUNTS_TABLE = "user_accounts";
    public static final String ACCOUNTS_COL_1 = "account_id";
    public static final String ACCOUNTS_COL_2 = "user_name";
    public static final String ACCOUNTS_COL_3 = "user_id";
    public static final String ACCOUNTS_COL_4 = "user_password";
    public static final String ACCOUNTS_COL_5 = "user_power";
    public static final String ACCOUNTS_COL_6 = "reg_date";
    public static final String ACCOUNTS_COL_7 = "user_status";



    private Context ctx;

    public DatabaseSQLite(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.ctx = context;
        SQLiteDatabase db = this.getWritableDatabase();
        checkTablesExist();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+"("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" VARCHAR(100), "+COL_3+" VARCHAR(700), "+COL_4+" VARCHAR(300), "+COL_5+" VARCHAR(800), "+COL_6+" FLOAT, "+COL_7+" VARCHAR(500), "+COL_8+" FLOAT, "+COL_9+" FLOAT, "+COL_10+" FLOAT, "+COL_11+" FLOAT, "+COL_12+" VARCHAR(300), "+COL_13+" VARCHAR(30), "+COL_14+" VARCHAR(30), "+COL_15+" VARCHAR(50), "+COL_16+" VARCHAR(30))");
        db.execSQL("create table "+SUBAREA_INFO_TABLE+"("+SUBAREA_COL_1+" INTEGER PRIMARY KEY, "+SUBAREA_COL_2+" INTEGER, "+SUBAREA_COL_3+" VARCHAR(200), "+SUBAREA_COL_4+" VARCHAR(50))");
        db.execSQL("create table "+DEALER_INFO_TABLE+"("+DEALER_COL_1+" INTEGER PRIMARY KEY, "+DEALER_COL_2+" INTEGER, "+DEALER_COL_3+" VARCHAR(150), "+DEALER_COL_4+" VARCHAR(50), "+DEALER_COL_5+" VARCHAR(200), "+DEALER_COL_6+" VARCHAR(50), "+DEALER_COL_7+" VARCHAR(50), "+DEALER_COL_8+" VARCHAR(100), "+DEALER_COL_9+" VARCHAR(50), "+DEALER_COL_10+" VARCHAR(50))");
        db.execSQL("create table "+PRODUCT_INFO_TABLE+"("+PRODUCT_COL_1+" INTEGER PRIMARY KEY, "+PRODUCT_COL_2+" VARCHAR(200), "+PRODUCT_COL_3+" FLOAT, "+PRODUCT_COL_4+" VARCHAR(50))");
        db.execSQL("create table "+USER_ACCOUNTS_TABLE+"("+ACCOUNTS_COL_1+" INTEGER PRIMARY KEY, "+ACCOUNTS_COL_2+" VARCHAR(100), "+ACCOUNTS_COL_3+" INTEGER, "+ACCOUNTS_COL_4+" VARCHAR(300), "+ACCOUNTS_COL_5+" VARCHAR(50), "+ACCOUNTS_COL_6+" VARCHAR(50), "+ACCOUNTS_COL_7+" VARCHAR(50))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+SUBAREA_INFO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DEALER_INFO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+PRODUCT_INFO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+USER_ACCOUNTS_TABLE);
        onCreate(db);
    }

    public void createOrderTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+TABLE_NAME+"("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" VARCHAR(100), "+COL_3+" VARCHAR(700), "+COL_4+" VARCHAR(300), "+COL_5+" VARCHAR(800), "+COL_6+" FLOAT, "+COL_7+" VARCHAR(500), "+COL_8+" FLOAT, "+COL_9+" FLOAT, "+COL_10+" FLOAT, "+COL_11+" FLOAT, "+COL_12+" VARCHAR(300), "+COL_13+" VARCHAR(30), "+COL_14+" VARCHAR(30), "+COL_15+" VARCHAR(50), "+COL_16+" VARCHAR(30))");
    }
    public void createSubareaTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+SUBAREA_INFO_TABLE+"("+SUBAREA_COL_1+" INTEGER PRIMARY KEY, "+SUBAREA_COL_2+" INTEGER, "+SUBAREA_COL_3+" VARCHAR(200), "+SUBAREA_COL_4+" VARCHAR(50))");
    }
    public void createDealerTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DEALER_INFO_TABLE+"("+DEALER_COL_1+" INTEGER PRIMARY KEY, "+DEALER_COL_2+" INTEGER, "+DEALER_COL_3+" VARCHAR(150), "+DEALER_COL_4+" VARCHAR(50), "+DEALER_COL_5+" VARCHAR(200), "+DEALER_COL_6+" VARCHAR(50), "+DEALER_COL_7+" VARCHAR(50), "+DEALER_COL_8+" VARCHAR(100), "+DEALER_COL_9+" VARCHAR(50), "+DEALER_COL_10+" VARCHAR(50))");
    }
    public void createProductTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+PRODUCT_INFO_TABLE+"("+PRODUCT_COL_1+" INTEGER PRIMARY KEY, "+PRODUCT_COL_2+" VARCHAR(200), "+PRODUCT_COL_3+" FLOAT, "+PRODUCT_COL_4+" VARCHAR(50))");
    }
    public void createUserAccountsTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+USER_ACCOUNTS_TABLE+"("+ACCOUNTS_COL_1+" INTEGER PRIMARY KEY, "+ACCOUNTS_COL_2+" VARCHAR(100), "+ACCOUNTS_COL_3+" VARCHAR(50), "+ACCOUNTS_COL_4+" VARCHAR(300), "+ACCOUNTS_COL_5+" VARCHAR(50), "+ACCOUNTS_COL_6+" VARCHAR(50), "+ACCOUNTS_COL_7+" VARCHAR(50))");
    }

    public boolean insertData(String dealerID, String productID, String quantity, String unit, String orderPrice, String bonus, String discount, String finalPrice, String latitude, String longitude, String order_place_area, String date, String time, String salesmanId, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, dealerID);
        contentValues.put(COL_3, productID);
        contentValues.put(COL_4, quantity);
        contentValues.put(COL_5, unit);
        contentValues.put(COL_6, orderPrice);
        contentValues.put(COL_7, bonus);
        contentValues.put(COL_8, discount);
        contentValues.put(COL_9, finalPrice);
        contentValues.put(COL_10, latitude);
        contentValues.put(COL_11, longitude);
        contentValues.put(COL_12, order_place_area);
        contentValues.put(COL_13, date);
        contentValues.put(COL_14, time);
        contentValues.put(COL_15, salesmanId);
        contentValues.put(COL_16, status);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean registerUser(String userName, String userId, String password, String userPower, String stDate, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCOUNTS_COL_2, userName);
        contentValues.put(ACCOUNTS_COL_3, userId);
        contentValues.put(ACCOUNTS_COL_4, password);
        contentValues.put(ACCOUNTS_COL_5, userPower);
        contentValues.put(ACCOUNTS_COL_6, stDate);
        contentValues.put(ACCOUNTS_COL_7, status);
        long result = db.insert(USER_ACCOUNTS_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateUser(String userName, String userId, String password, String userPower, String stDate, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCOUNTS_COL_2, userName);
        contentValues.put(ACCOUNTS_COL_3, userId);
        contentValues.put(ACCOUNTS_COL_4, password);
        contentValues.put(ACCOUNTS_COL_5, userPower);
        contentValues.put(ACCOUNTS_COL_6, stDate);
        contentValues.put(ACCOUNTS_COL_7, status);
        long result = db.update(USER_ACCOUNTS_TABLE, contentValues, "user_id="+userId, null);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateData(String orderId, String dealerID, String productID, String quantity, String unit, String orderPrice, String bonus, String discount, String finalPrice, String latitude, String longitude, String order_place_area, String date, String time, String salesmanId, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, dealerID);
        contentValues.put(COL_3, productID);
        contentValues.put(COL_4, quantity);
        contentValues.put(COL_5, unit);
        contentValues.put(COL_6, orderPrice);
        contentValues.put(COL_7, bonus);
        contentValues.put(COL_8, discount);
        contentValues.put(COL_9, finalPrice);
        contentValues.put(COL_10, latitude);
        contentValues.put(COL_11, longitude);
        contentValues.put(COL_12, order_place_area);
        contentValues.put(COL_13, date);
        contentValues.put(COL_14, time);
        contentValues.put(COL_15, salesmanId);
        contentValues.put(COL_16, status);
        int result = db.update(TABLE_NAME, contentValues, "id="+orderId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Cursor getAllData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        return res;

    }

    public void checkTablesExist()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        if(!doesTableExist(db, TABLE_NAME))
        {
            createOrderTable(db);
        }

        if(!doesTableExist(db, SUBAREA_INFO_TABLE))
        {
            createSubareaTable(db);
        }

        if(!doesTableExist(db, DEALER_INFO_TABLE))
        {
            createDealerTable(db);
        }

        if(!doesTableExist(db, PRODUCT_INFO_TABLE))
        {
            createProductTable(db);
        }

        if(!doesTableExist(db, USER_ACCOUNTS_TABLE))
        {
            createUserAccountsTable(db);
        }
    }

    public Cursor getPending()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT order_info.id, dealer_info.dealer_name, order_info.final_price, order_info.date FROM "+TABLE_NAME+" INNER JOIN dealer_info ON dealer_info.dealer_id=order_info.dealer_id WHERE order_info.status = 'Pending'", null);
        return res;
    }

    public boolean updateLocal()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("UPDATE "+TABLE_NAME+" SET status='SENT' WHERE status = 'Pending'", null);
        return true;
    }

    public Cursor getOrderInfo(String orderId)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT order_info.*, dealer_info.dealer_name, dealer_info.dealer_address FROM "+TABLE_NAME+" INNER JOIN dealer_info ON dealer_info.dealer_id=order_info.dealer_id WHERE order_info.id = "+orderId, null);
        return res;
    }

    public Cursor getProductInfo()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM product_info WHERE product_status = 'Active'", null);
        return res;
    }

    public Cursor getRegistration()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM "+USER_ACCOUNTS_TABLE, null);
        return res;
    }

    public void deleteAccount()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseSQLite.USER_ACCOUNTS_TABLE, null, null);
    }

    private static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public boolean doesTableExist(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }


}
