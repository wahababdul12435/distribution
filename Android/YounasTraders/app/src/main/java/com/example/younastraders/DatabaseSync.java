package com.example.younastraders;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DatabaseSync extends SQLiteOpenHelper {
    String[][] subAreaInfo;
    String[][] dealerInfo;
    String[][] productInfo;

    public static final String DATABASE_NAME = "YounasTraders.db";


    public Context context;

    public DatabaseSync(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void deletaAllData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseSQLite.SUBAREA_INFO_TABLE, null, null);
        db.delete(DatabaseSQLite.DEALER_INFO_TABLE, null, null);
        db.delete(DatabaseSQLite.PRODUCT_INFO_TABLE, null, null);
    }

    public boolean insertSubarea(String subareaId, String areaID, String subareaName, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.SUBAREA_COL_1, subareaId);
        contentValues.put(DatabaseSQLite.SUBAREA_COL_2, areaID);
        contentValues.put(DatabaseSQLite.SUBAREA_COL_3, subareaName);
        contentValues.put(DatabaseSQLite.SUBAREA_COL_4, status);
        long result = db.insert(DatabaseSQLite.SUBAREA_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealer(String dealerId, String dealerAreaId, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCnic, String dealerLicNum, String dealerLicExp, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALER_COL_1, dealerId);
        contentValues.put(DatabaseSQLite.DEALER_COL_2, dealerAreaId);
        contentValues.put(DatabaseSQLite.DEALER_COL_3, dealerName);
        contentValues.put(DatabaseSQLite.DEALER_COL_4, dealerContact);
        contentValues.put(DatabaseSQLite.DEALER_COL_5, dealerAddress);
        contentValues.put(DatabaseSQLite.DEALER_COL_6, dealerType);
        contentValues.put(DatabaseSQLite.DEALER_COL_7, dealerCnic);
        contentValues.put(DatabaseSQLite.DEALER_COL_8, dealerLicNum);
        contentValues.put(DatabaseSQLite.DEALER_COL_9, dealerLicExp);
        contentValues.put(DatabaseSQLite.DEALER_COL_10, status);
        long result = db.insert(DatabaseSQLite.DEALER_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertProduct(String productId, String productName, String finalPrice, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.PRODUCT_COL_1, productId);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_2, productName);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_3, finalPrice);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_4, status);
        long result = db.insert(DatabaseSQLite.PRODUCT_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void insertAllRecord()
    {
        deletaAllData();
        final boolean[] subareaResponse = {false};
        final boolean[] dealerResponse = {false};
        final boolean[] productResponse = {false};

        final FetchDataMysql objfetchmysql1= new FetchDataMysql(context, "subarea_info");
        objfetchmysql1.setCallBack(new FetchDataMysql.CallBack() {

            public void onResult(String value) {
                subAreaInfo = objfetchmysql1.getSubAreaInfo();
                if(subAreaInfo != null)
                {
                    for(int i=0; i<subAreaInfo.length; i++)
                    {
                        subareaResponse[0] = insertSubarea(subAreaInfo[i][0], subAreaInfo[i][1], subAreaInfo[i][2], subAreaInfo[i][3]);
                    }
                    Toast.makeText(context, "Synced From Server", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(context, "Unable to Connect to Server", Toast.LENGTH_LONG).show();
                }
            }
        });

        final FetchDataMysql objfetchmysql2 = new FetchDataMysql(context,"dealer_info");
        objfetchmysql2.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                dealerInfo = objfetchmysql2.getDealerInfo();
                if(dealerInfo != null)
                {
                    for(int i=0; i<dealerInfo.length; i++)
                    {
                        dealerResponse[0] = insertDealer(dealerInfo[i][0], dealerInfo[i][1], dealerInfo[i][2], dealerInfo[i][3], dealerInfo[i][4], dealerInfo[i][5], dealerInfo[i][6], dealerInfo[i][7], dealerInfo[i][8], dealerInfo[i][9]);
                    }
                }

            }
        });


        final FetchDataMysql objfetchmysql3 = new FetchDataMysql(context,"product_info");
        objfetchmysql3.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                productInfo = objfetchmysql3.getProductInfo();
                if(productInfo != null)
                {
                    for(int i=0; i<productInfo.length; i++)
                    {
                        productResponse[0] = insertProduct(productInfo[i][0], productInfo[i][1], productInfo[i][2], productInfo[i][3]);
                    }
                }
            }
        });
    }

//    public void showMessage(String title, String message)
//    {
//        AlertDialog.Builder objBuilder = new AlertDialog.Builder(this.context);
//        objBuilder.setCancelable(true);
//        objBuilder.setTitle(title);
//        objBuilder.setMessage(message);
//        objBuilder.show();
//    }

//    public Cursor getAllData()
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
//        return res;
//    }
//
//    public Cursor getPending()
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE STATUS = 'Pending'", null);
//        return res;
//    }
//
//    public boolean updateLocal()
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor res = db.rawQuery("UPDATE "+TABLE_NAME+" SET status='SENT' WHERE status = 'Pending'", null);
//        return true;
//    }
}
