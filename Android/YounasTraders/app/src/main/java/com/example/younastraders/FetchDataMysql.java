package com.example.younastraders;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class FetchDataMysql {

    String[][] subAreaInfo;
    String[][] dealerInfo;
    String[][] productInfo;
    String dataTableName;
    Context ctx;

    public FetchDataMysql(Context ctx, String dataTableName) {
        this.ctx = ctx;
        this.dataTableName = dataTableName;
        getJSON("http://10.0.2.2/GitKraken/distribution/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);
    }

    private CallBack callBack;

    public CallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }


    public void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(callBack != null && s != null)
                {
                    try {
                        loadIntoListView(s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
//                else
//                {
//                    Toast.makeText(ctx, "Unable To Connect To Server", Toast.LENGTH_LONG).show();
//                }
                callBack.onResult(s);
            }

            @Override
            protected String doInBackground(Void... voids) {
                try
                {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setConnectTimeout(2 * 1000);
                    con.connect();
                    if (con.getResponseCode() == 200)
                    {
                        StringBuilder sb = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String json;
                        while ((json = bufferedReader.readLine()) != null)
                        {
                            sb.append(json + "\n");
                        }
                        return sb.toString().trim();
                    }
                    else
                    {
//                        Toast.makeText(ctx, "Unable To Connect", Toast.LENGTH_LONG).show();
                        return null;
                    }
//                    StringBuilder sb = new StringBuilder();
//                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
//                    String json;
//                    while ((json = bufferedReader.readLine()) != null)
//                    {
//                        sb.append(json + "\n");
//                    }
//                    return sb.toString().trim();
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        if(dataTableName.equals("subarea_info"))
        {
            subAreaInfo = new String[jsonArray.length()][4];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                subAreaInfo[i][0] = obj.getString("id");
                subAreaInfo[i][1] = obj.getString("areaid");
                subAreaInfo[i][2] = obj.getString("subarea_name");
                subAreaInfo[i][3] = obj.getString("subarea_status");
            }
        }
        else if(dataTableName.equals("dealer_info"))
        {
            dealerInfo = new String[jsonArray.length()][10];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                dealerInfo[i][0] = obj.getString("id");
                dealerInfo[i][1] = obj.getString("areaid");
                dealerInfo[i][2] = obj.getString("dealer_name");
                dealerInfo[i][3] = obj.getString("dealer_contact");
                dealerInfo[i][4] = obj.getString("dealer_address");
                dealerInfo[i][5] = obj.getString("dealer_type");
                dealerInfo[i][6] = obj.getString("dealer_cnic");
                dealerInfo[i][7] = obj.getString("dealer_licnum");
                dealerInfo[i][8] = obj.getString("dealer_licexp");
                dealerInfo[i][9] = obj.getString("dealer_status");
            }
        }
        else if(dataTableName.equals("product_info"))
        {
            productInfo = new String[jsonArray.length()][4];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                productInfo[i][0] = obj.getString("id");
                productInfo[i][1] = obj.getString("product_name");
                productInfo[i][2] = obj.getString("final_price");
                productInfo[i][3] = obj.getString("product_status");
            }
        }
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, heroes);
//        listView.setAdapter(arrayAdapter);
    }

    public String[][] getSubAreaInfo() {
        return subAreaInfo;
    }

    public String[][] getDealerInfo() {
        return dealerInfo;
    }

    public String[][] getProductInfo() {
        return productInfo;
    }

    public interface CallBack{
        void onResult(String value);
    }
}