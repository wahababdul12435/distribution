package com.example.younastraders;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    TextView newSignUp;
    DatabaseSQLite objsqlite;
    TextView txtUserName;
    TextView txtUserId;
    TextView txtUserStatus;
    Button btnReSend;
    SendData objSendData;
    ProgressBar progressReg;

    String stUserName;
    String stUserId;
    String stUserPass;
    String stUserPower;
    String stRegDate;
    String stUserStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        objsqlite = new DatabaseSQLite(this);
        txtUserName = findViewById(R.id.txt_username);
        txtUserId = findViewById(R.id.txt_userid);
        txtUserStatus = findViewById(R.id.txt_userstatus);
        newSignUp = findViewById(R.id.new_sign_up);
        btnReSend = findViewById(R.id.btnReSend);
        progressReg = findViewById(R.id.progressReg);
        objSendData = new SendData(this);
        viewReg();
        newSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnReSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressReg.setVisibility(View.VISIBLE);
                String responseStatus = objSendData.reSendRegisterUser(stUserName, stUserId, stUserPass, stUserPower, stRegDate);
            }
        });
    }

    public void viewReg()
    {
        Cursor res = objsqlite.getRegistration();

        if(res.getCount() == 0)
        {
            findViewById(R.id.txt_regheader).setVisibility(View.GONE);
            findViewById(R.id.txt_username_fix).setVisibility(View.GONE);
            findViewById(R.id.txt_userid_fix).setVisibility(View.GONE);
            findViewById(R.id.txt_userstatus_fix).setVisibility(View.GONE);
            btnReSend.setVisibility(View.GONE);
        }
        else
        {
            res.moveToNext();
            stUserName = res.getString(1);
            stUserId = res.getString(2);
            stUserPass = res.getString(3);
            stUserPower = res.getString(4);
            stRegDate = res.getString(5);
            stUserStatus = res.getString(6);
            txtUserName.setText(stUserName);
            txtUserId.setText(stUserId);
            txtUserStatus.setText(stUserStatus);
            if(!stUserStatus.equals("Not Sent"))
            {
                btnReSend.setVisibility(View.GONE);
            }
        }
    }
}
