package com.example.younastraders;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.younastraders.ui.viewpending.ViewPendingOrdersFragment;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PendingOrderDetail extends AppCompatActivity {

    int itemsCount;
    int maxExtraProducts = 50;
    float orderPrice = 0;
    String bonus = "";
    float discount = 0;
    float finalPrice = 0;
    Button btnSubmitOrder;
    Button btnSaveOrder;
    SearchableSpinner edDealerID;

    Spinner[] orgProducts;
    EditText[] orgqQuantity;
    Spinner[] orgUnit;

    LinearLayout[] subLayout;
    SearchableSpinner[] extraProducts;
    EditText[] extraQuantity;
    Spinner[] extraUnits;
    Button[] extraOpBtn;
    ProgressBar edProgress;
    SendData objSendData;
    Button btnAdd;
    Button btnEdit;
    EditText edOrderPrice;
    EditText edDiscount;
    EditText edFinalPrice;

    ArrayAdapter<String> dealerAdapter;
    ArrayAdapter<String> productAdapter;
    ArrayAdapter<String> unitAdapter;
    LinearLayout lv;

    DatabaseSQLite objDatabaseSqlite;

    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersAddress;
    public String[] productsIds;
    public String[] productsName;
    public float[] productsPrice;

    String orderID;

    String stDate;
    String stTime;

    String getDealerName;
//    String getDealerId;
    String getProductId;
    String getQuantity;
    String getUnit;
    String getOrderPrice;
    String getBonus;
    String getDiscount;
    String getFinalPrice;
    String getDate;
    String getTime;


    ArrayList<String> allProductsIds;
    ArrayList<String> allProductsNames;
    ArrayList<String> allProductsPrices;

    String lat;
    String lon;
    String orderPlaceArea;

    String send_dealerId;
    String send_productIds = "";
    String send_quantities = "";
    String send_unit = "";
    String send_salesmanId = "1";

    boolean priceCal = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_order_detail);
        edOrderPrice = findViewById(R.id.edorderprice);
        edDiscount = findViewById(R.id.eddiscount);
        edDiscount.setText("0.0");
        edFinalPrice = findViewById(R.id.edfinalprice);

        orderID = getIntent().getStringExtra("ORDER_ID");
        objDatabaseSqlite = new DatabaseSQLite(this);

        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        this.stDate = df.format(objDate);
        this.stTime = sdf.format(objDate);
        DealersAndProductsInfo objdealandproinfo = new DealersAndProductsInfo(this);
        dealersId = objdealandproinfo.getDealersId();
        dealersName = objdealandproinfo.getDealersName();
        dealersAddress = objdealandproinfo.getDealersAddress();
        productsIds = objdealandproinfo.getProductsIds();
        productsName = objdealandproinfo.getProductsName();
        productsPrice = objdealandproinfo.getProductsPrice();
        orgProducts = new Spinner[4];
        orgqQuantity = new EditText[4];
        orgUnit = new Spinner[4];

        subLayout = new LinearLayout[maxExtraProducts];
        extraProducts = new SearchableSpinner[maxExtraProducts];
        extraQuantity = new EditText[maxExtraProducts];
        extraUnits = new Spinner[maxExtraProducts];
        extraOpBtn = new Button[maxExtraProducts];

        objSendData = new SendData(this);
        itemsCount = 0;
        btnAdd = findViewById(R.id.button_add);
        btnEdit = findViewById(R.id.btn_edit);
        edDealerID = findViewById(R.id.ed_dealerName);
        lv = findViewById(R.id.lnear_layout);

        dealerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerID.setAdapter(dealerAdapter);

        productAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, productsName);

        String[] unitItems = new String[]{"Packets", "Box"};
        unitAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, unitItems);

        btnSaveOrder = findViewById(R.id.btn_save);
        btnSubmitOrder = findViewById(R.id.btn_submitorder);


        Cursor res = objDatabaseSqlite.getProductInfo();
        if(res.getCount() == 0)
        {

        }
        else
        {
            allProductsIds = new ArrayList<String>(res.getCount());
            allProductsNames = new ArrayList<String>(res.getCount());
            allProductsPrices = new ArrayList<String>(res.getCount());
            while(res.moveToNext())
            {
                allProductsIds.add(res.getString(0));
                allProductsNames.add(res.getString(1));
                allProductsPrices.add(res.getString(2));
            }
        }


        Cursor res1 = objDatabaseSqlite.getOrderInfo(orderID);
        if(res1.getCount() == 0)
        {

        }
        else
        {
            while(res1.moveToNext())
            {
                getDealerName = res1.getString(16)+" ("+res1.getString(17)+")";
                int dealerPos = dealerAdapter.getPosition(getDealerName);
                edDealerID.setSelection(dealerPos);
                edDealerID.setEnabled(false);
//                getDealerId = res1.getString(1);
                getProductId = res1.getString(2);
                getQuantity = res1.getString(3);
                getUnit = res1.getString(4);
                getOrderPrice = res1.getString(5);
                getBonus = res1.getString(6);
                getDiscount = res1.getString(7);
                getFinalPrice = res1.getString(8);
                lat = res1.getString(9);
                lon = res1.getString(10);
                orderPlaceArea = res1.getString(11);
                getDate = res1.getString(12);
                getTime = res1.getString(13);
                String[] productArr = getProductId.split("_-_");
                String[] quantityArr = getQuantity.split("_-_");
                String[] unitArr = getUnit.split("_-_");
                for(int j=0; j<productArr.length; j++)
                {
                    int proIndex = allProductsIds.indexOf(productArr[j]);
                    String proName = allProductsNames.get(proIndex);
                    itemsCount++;
                    lv.addView(createLinearLayout(proName, quantityArr[j], unitArr[j]));
                }
                addListenerToButtons();
            }
            edOrderPrice.setText(getOrderPrice);
            edDiscount.setText(getDiscount);
            edFinalPrice.setText(getFinalPrice);
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemsCount++;
                lv.addView(createLinearLayout("Select Item", "1", "Packets"));
                addListenerToButtons();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = btnEdit.getText().toString();
                if(text.equals("Edit"))
                {
                    btnEdit.setText("Cancel");
                    edDealerID.setEnabled(true);
                    btnAdd.setEnabled(true);
                    for(int i=0; i<itemsCount; i++)
                    {
                        extraProducts[i].setEnabled(true);
                        extraQuantity[i].setEnabled(true);
                        extraUnits[i].setEnabled(true);
                        extraOpBtn[i].setEnabled(true);
                        priceCal = true;
                    }
                }
                else
                {
                    finish();
                }

            }
        });


        edProgress = findViewById(R.id.ed_progress);

        btnSaveOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compileOrder();
                String returnStatus = objSendData.updateCurrent(orderID, send_dealerId, send_productIds, send_quantities, send_unit, String.valueOf(orderPrice), bonus, String.valueOf(discount), String.valueOf(finalPrice), lat, lon, orderPlaceArea, getDate, getTime, send_salesmanId);
//                FragmentManager manager = getSupportFragmentManager();
//                ViewPendingOrdersFragment objPendingFragment = new ViewPendingOrdersFragment();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.detach(objPendingFragment);
//                transaction.attach(objPendingFragment);
//                transaction.replace(R.id.pending_order,objPendingFragment).commit();

//                FragmentManager manager = getSupportFragmentManager();
//                Fragment frg = null;
//                frg = manager.findFragmentByTag("ViewPendingOrdersFragment");
//                final FragmentTransaction ft = manager.beginTransaction();
//                ft.detach(frg);
//                ft.attach(frg);
//                ft.commit();

                Intent intent = new Intent(PendingOrderDetail.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        btnSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compileOrder();
                String returnStatus = objSendData.sendCurrent(send_dealerId, send_productIds, send_quantities, send_unit, String.valueOf(orderPrice), bonus, String.valueOf(discount), String.valueOf(finalPrice), lat, lon, orderPlaceArea, getDate, getTime, send_salesmanId);
                Intent intent = new Intent(PendingOrderDetail.this, Home.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void compileOrder()
    {
        boolean check = false;
        if(edDealerID.getSelectedItemPosition() != 0)
        {
            send_dealerId = dealersId[edDealerID.getSelectedItemPosition()];
        }
        else
        {
            return;
        }
        for(int j=0; j<itemsCount; j++)
        {
            if(!(subLayout[j].getVisibility() == View.GONE) && extraProducts[j].getSelectedItemPosition() != 0)
            {
                if(check)
                {
                    send_productIds = send_productIds+"_-_"+productsIds[extraProducts[j].getSelectedItemPosition()];
                    send_quantities = send_quantities+"_-_"+extraQuantity[j].getText().toString();
                    send_unit = send_unit+"_-_Packets";
                }
                else
                {
                    send_productIds = productsIds[extraProducts[j].getSelectedItemPosition()];
                    send_quantities = extraQuantity[j].getText().toString();
                    send_unit = "Packets";
                    check = true;
                }

            }
        }
        orderPrice = Float.parseFloat(edOrderPrice.getText().toString());
        discount = Float.parseFloat(edDiscount.getText().toString());
        finalPrice = Float.parseFloat(edFinalPrice.getText().toString());
        edProgress = findViewById(R.id.ed_progress);
        edProgress.setVisibility(View.VISIBLE);
    }

    public SearchableSpinner createProductSpinner(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.product_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraProducts[itemsCount-1] = new SearchableSpinner(PendingOrderDetail.this);
        extraProducts[itemsCount-1].setLayoutParams(lparams);
        extraProducts[itemsCount-1].setAdapter(productAdapter);
        int pos = productAdapter.getPosition(text);
        extraProducts[itemsCount-1].setSelection(pos);
        if(!priceCal)
        {
            extraProducts[itemsCount-1].setEnabled(false);
        }
        return extraProducts[itemsCount-1];
    }
    public EditText createQuantityEditText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.quantity_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraQuantity[itemsCount-1] = new EditText(PendingOrderDetail.this);
        extraQuantity[itemsCount-1].setLayoutParams(lparams);
        extraQuantity[itemsCount-1].setMovementMethod(new ScrollingMovementMethod());
        extraQuantity[itemsCount-1].setText(text);
        extraQuantity[itemsCount-1].setInputType(InputType.TYPE_CLASS_NUMBER);
        extraQuantity[itemsCount-1].setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        if(!priceCal)
        {
            extraQuantity[itemsCount-1].setEnabled(false);
        }
        return extraQuantity[itemsCount-1];
    }
    public Spinner createUnitSpinner(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.unit_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraUnits[itemsCount-1] = new Spinner(PendingOrderDetail.this);
        extraUnits[itemsCount-1].setLayoutParams(lparams);
        extraUnits[itemsCount-1].setAdapter(unitAdapter);
        int pos = unitAdapter.getPosition(text);
        extraUnits[itemsCount-1].setSelection(pos);
        if(!priceCal)
        {
            extraUnits[itemsCount-1].setEnabled(false);
        }
        return extraUnits[itemsCount-1];
    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        extraOpBtn[itemsCount-1] = new Button(PendingOrderDetail.this);
        extraOpBtn[itemsCount-1].setLayoutParams(lparams);
        extraOpBtn[itemsCount-1].setText("X");
        extraOpBtn[itemsCount-1].setTextColor(Color.RED);
        extraOpBtn[itemsCount-1].setBackgroundColor(Color.TRANSPARENT);
        if(!priceCal)
        {
            extraOpBtn[itemsCount-1].setEnabled(false);
        }
        return extraOpBtn[itemsCount-1];
    }

    public LinearLayout createLinearLayout(String productName, String quantity, String unit) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[itemsCount-1] = new LinearLayout(PendingOrderDetail.this);
        subLayout[itemsCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[itemsCount-1].setLayoutParams(lparams);
        subLayout[itemsCount-1].addView(createProductSpinner(productName));
        subLayout[itemsCount-1].addView(createQuantityEditText(quantity));
        subLayout[itemsCount-1].addView(createUnitSpinner(unit));
        subLayout[itemsCount-1].addView(createOpButton());
        return subLayout[itemsCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<itemsCount; i++)
        {
            final int finalI = i;
            extraOpBtn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subLayout[finalI].setVisibility(View.GONE);
                    calculatePrice();
                }
            });

            extraProducts[i].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    calculatePrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            extraQuantity[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        if(extraQuantity[finalI].getText().toString().equals(""))
                        {
                            extraQuantity[finalI].setText("1");
                        }
                        calculatePrice();
                    }
                }
            });

            extraQuantity[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    calculatePrice();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    calculatePrice();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    calculatePrice();
                }
            });
        }
        calculatePrice();
    }

    public void calculatePrice()
    {
        if(priceCal)
        {
            orderPrice = 0f;
            finalPrice = 0f;
            for(int j=0; j<itemsCount; j++)
            {
                if(!(subLayout[j].getVisibility() == View.GONE))
                {
                    String stQan = extraQuantity[j].getText().toString();
                    if(stQan.equals(""))
                    {
                        stQan = "0";
                    }
                    orderPrice = orderPrice + (productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                }
            }
            discount = Float.valueOf(edDiscount.getText().toString());
            finalPrice = orderPrice - ((orderPrice/100)*discount);
            edOrderPrice.setText(String.valueOf(orderPrice));
            edFinalPrice.setText(String.valueOf(finalPrice));
        }

    }
}
