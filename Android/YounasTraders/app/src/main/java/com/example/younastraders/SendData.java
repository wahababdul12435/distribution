package com.example.younastraders;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class SendData {
    String status;
    BackgroundTask objBackground;
    DatabaseSQLite objDatabaseSqlite;
    Context ctx;
    String responseStatus;

    public SendData(Context ctx)
    {
        this.ctx = ctx;
        this.objDatabaseSqlite = new DatabaseSQLite(ctx);
        this.objBackground = new BackgroundTask(ctx);
        this.status = "Pending";
        this.responseStatus = "";
    }

    public String sendCurrent(final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        MainActivity.edProgress.setVisibility(View.VISIBLE);
        objBackground.setCallBack(new BackgroundTask.CallBack() {
            @Override
            public void onResult(String value) {
                if(value == "Order Sent Successfully")
                {
                    status = "Sent";
                    responseStatus = "Order Sent ";
                }
                else
                {
                    status = "Pending";
                    responseStatus = "Order Not Sent ";
                }
                boolean result = objDatabaseSqlite.insertData(dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
                if(result == true)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and Not Saved";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        objBackground.execute("order", dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        return responseStatus;
    }

    public String saveCurrent(final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        MainActivity.edProgress.setVisibility(View.VISIBLE);
        status = "Pending";
        boolean result = objDatabaseSqlite.insertData(dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        if(result == true)
        {
            responseStatus = "Order Saved";
        }
        else
        {
            responseStatus = "Order Not Saved";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
        return responseStatus;
    }

    public String updateCurrent(final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.updateData(orderId, dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        if(result == true)
        {
            responseStatus = "Order Updated";
        }
        else
        {
            responseStatus = "Order Not Updated";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        return responseStatus;
    }

    public String registerUser(final String userName, final String userId, final String password, final String userPower, final String stDate)
    {
        objBackground.setCallBack(new BackgroundTask.CallBack() {
            @Override
            public void onResult(String value) {
                if(value == "Registration Request Sent Successfully")
                {
                    status = "Pending";
                    responseStatus = "Registration Request Sent";
                }
                else
                {
                    status = "Not Sent";
                    responseStatus = "Registration Request Not Sent";
                }
                boolean result = objDatabaseSqlite.registerUser(userName, userId, password, userPower, stDate, status);
                if(result == true)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and unable to Save";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        objBackground.execute("register", userName, userId, password, userPower, stDate,  status);
        return responseStatus;
    }

    public String reSendRegisterUser(final String userName, final String userId, final String password, final String userPower, final String stDate)
    {
        objBackground.setCallBack(new BackgroundTask.CallBack() {
            @Override
            public void onResult(String value) {
                boolean result = objDatabaseSqlite.registerUser(userName, userId, password, userPower, stDate, status);
                if(value == "Registration Request Sent Successfully")
                {
                    status = "Pending";
                    responseStatus = "Registration Request Sent";
                    result = objDatabaseSqlite.updateUser(userName, userId, password, userPower, stDate, status);
                }
                else
                {
                    status = "Not Sent";
                    responseStatus = "Registration Request Not Sent";
                }
                if(result == true)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and unable to Save";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        objBackground.execute("register", userName, userId, password, userPower, stDate,  status);
        return responseStatus;
    }

    public String sendPendings()
    {
        String dealerId;
        String productId;
        String quantity;
        String unit;
        String orderPrice;
        String bonus;
        String discount;
        String finalPrice;
        String latitude;
        String longitude;
        String order_place_area;
        String salessmanId;
        String date;
        String time;
        String status;
        String returnResponse = "";
        int i = 0;
        // on Button Click
        Cursor res = objDatabaseSqlite.getAllData();

        if(res.getCount() == 0)
        {
            return "Orders already Submitted";
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            while(res.moveToNext())
            {
                dealerId = res.getString(1);
                productId = res.getString(2);
                quantity = res.getString(3);
                unit = res.getString(4);
                orderPrice = res.getString(5);
                bonus = res.getString(6);
                discount = res.getString(7);
                finalPrice = res.getString(8);
                latitude = res.getString(9);
                longitude = res.getString(10);
                order_place_area = res.getString(11);
                date = res.getString(12);
                time = res.getString(13);
                salessmanId = res.getString(14);
                status = "Pending";

//                objBuffer.append("id: "+res.getString(0)+"\n");
//                objBuffer.append("dealer_id: "+res.getString(1)+"\n");
//                objBuffer.append("product_id: "+res.getString(2)+"\n");
//                objBuffer.append("quantity: "+res.getString(3)+"\n");
//                objBuffer.append("status: "+res.getString(4)+"\n");

                returnResponse = sendCurrent(dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, date, time, salessmanId);
                if(returnResponse == "Order Not Sent ")
                {
                    return returnResponse;
                }
            }
            if(i == res.getCount())
            {
                if(updateLocal())
                {
                    return "All Pending Orders Submitted";
                }
                else
                {
                    return "Orders Submitted But Status Still Pending";
                }
            }
            else
            {
                return "Incomplete Orders Submission";
            }
        }
    }

    public boolean updateLocal()
    {
        if(objDatabaseSqlite.updateLocal())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
