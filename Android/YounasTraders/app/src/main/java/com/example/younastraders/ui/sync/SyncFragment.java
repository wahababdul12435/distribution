package com.example.younastraders.ui.sync;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.younastraders.DatabaseSync;
import com.example.younastraders.Home;
import com.example.younastraders.R;

public class SyncFragment extends Fragment {

    private SyncViewModel shareViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        shareViewModel = ViewModelProviders.of(this).get(SyncViewModel.class);
        View root = inflater.inflate(R.layout.fragment_sync, container, false);
//        final TextView textView = root.findViewById(R.id.text_share);
//        shareViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
//        boolean response;
        DatabaseSync objDatabaseSync = new DatabaseSync(getContext());
        objDatabaseSync.insertAllRecord();
        Intent intent = new Intent(getActivity(), Home.class);
        startActivity(intent);
        return root;
    }
}