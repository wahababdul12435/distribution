package com.example.younastraders.ui.viewpending;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.InputFilterMinMax;
import com.example.younastraders.MainActivity;
import com.example.younastraders.PendingOrderDetail;
import com.example.younastraders.R;
import com.example.younastraders.ui.home.HomeFragment;

import org.w3c.dom.Text;

public class ViewPendingOrdersFragment extends Fragment {

    private ViewPendingOrdersViewModel objViewPendingViewModel;
    LinearLayout lv;
    LinearLayout[] subLayout;
    int ordersCount;

    DatabaseSQLite objSqlLite;

    String[] txOrderId;
    TextView[] txDealerName;
    TextView[] txFinalPrice;
    TextView[] txDate;
    Button[] btnOps;

    private static final String FRAGMENT_NAME = ViewPendingOrdersFragment.class.getSimpleName();
    private static final String TAG = FRAGMENT_NAME;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objViewPendingViewModel = ViewModelProviders.of(this).get(ViewPendingOrdersViewModel.class);
        View root = inflater.inflate(R.layout.fragment_viewpendingorders, container, false);
        objSqlLite = new DatabaseSQLite(getActivity());
        ordersCount = 0;
        lv = root.findViewById(R.id.lnear_layout);
        viewAll();
        objViewPendingViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
//        Toast.makeText(getActivity(), TAG, Toast.LENGTH_LONG).show();
        return root;
    }

    public void viewAll()
    {
        Cursor res = objSqlLite.getPending();

        if(res.getCount() == 0)
        {
            txDealerName = new TextView[1];
            lv.addView(createEmptyText());
        }
        else
        {
            subLayout = new LinearLayout[res.getCount()];
            txOrderId = new String[res.getCount()];
            txDealerName = new TextView[res.getCount()];
            txFinalPrice = new TextView[res.getCount()];
            txDate = new TextView[res.getCount()];
            btnOps = new Button[res.getCount()];
            while(res.moveToNext())
            {
                ordersCount++;
                txOrderId[ordersCount-1] = res.getString(0);
                lv.addView(createLinearLayout(res.getString(1), res.getString(2), res.getString(3)));
                addListenerToButtons();
            }
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txDealerName[0] = new TextView(getActivity());
        txDealerName[0].setLayoutParams(lparams);
        txDealerName[0].setText("No Record to Show");
        txDealerName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txDealerName[0].setGravity(Gravity.CENTER);
        return txDealerName[0];
    }

    public TextView createDealerText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.pending_dealer_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        txDealerName[ordersCount-1] = new TextView(getActivity());
        txDealerName[ordersCount-1].setLayoutParams(lparams);
//        txDealerName[ordersCount-1].setBackgroundColor(Color.RED);
        txDealerName[ordersCount-1].setText(text);
        return txDealerName[ordersCount-1];
    }

    public TextView createFinalPriceText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.pending_final_price)), Math.round(getResources().getDimension(R.dimen.control_height)));
        txFinalPrice[ordersCount-1] = new TextView(getActivity());
        txFinalPrice[ordersCount-1].setLayoutParams(lparams);
//        txFinalPrice[ordersCount-1].setBackgroundColor(Color.GREEN);
        txFinalPrice[ordersCount-1].setText(text);
        return txFinalPrice[ordersCount-1];
    }
    public TextView createDateText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.pending_date)), Math.round(getResources().getDimension(R.dimen.control_height)));
        txDate[ordersCount-1] = new TextView(getActivity());
        txDate[ordersCount-1].setLayoutParams(lparams);
//        txDate[ordersCount-1].setBackgroundColor(Color.YELLOW);
        txDate[ordersCount-1].setText(text);
        return txDate[ordersCount-1];
    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        btnOps[ordersCount-1] = new Button(getActivity());
        btnOps[ordersCount-1].setLayoutParams(lparams);
        btnOps[ordersCount-1].setText("Detail");
//        btnOps[ordersCount-1].setTextColor(Color.RED);
        btnOps[ordersCount-1].setBackgroundColor(Color.TRANSPARENT);
        return btnOps[ordersCount-1];
    }

    public LinearLayout createLinearLayout(String dealerName, String finalPrice, String date) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[ordersCount-1] = new LinearLayout(getActivity());
        subLayout[ordersCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[ordersCount-1].setLayoutParams(lparams);
        subLayout[ordersCount-1].addView(createDealerText(dealerName));
        subLayout[ordersCount-1].addView(createFinalPriceText(finalPrice));
        subLayout[ordersCount-1].addView(createDateText(date));
        subLayout[ordersCount-1].addView(createOpButton());
        return subLayout[ordersCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<ordersCount; i++)
        {
            final int finalI = i;
            btnOps[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    subLayout[finalI].setVisibility(View.GONE);
                    Intent intent = new Intent(getActivity(), PendingOrderDetail.class);
                    intent.putExtra("ORDER_ID", txOrderId[finalI]);
                    startActivity(intent);
                }
            });
        }
    }
}