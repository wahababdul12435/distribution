package com.example.younastraders.ui.viewpending;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ViewPendingOrdersViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ViewPendingOrdersViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}