package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.CompanyInfo;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BonusPolicy implements Initializable {
    @FXML private TableView<CompanyInfo> company_table;
    @FXML private TableColumn<CompanyInfo, String> company_name;
    @FXML private TableColumn<CompanyInfo, Integer> company_policies;
    @FXML
    private Button cancelBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    public void initialize(URL location, ResourceBundle resources) {
        company_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        company_policies.setCellValueFactory(new PropertyValueFactory<>("totalProducts"));

        company_table.getItems().setAll(parseUserList());
    }
    private List<CompanyInfo> parseUserList(){
        ObservableList<CompanyInfo> companies = FXCollections.observableArrayList();
        companies.add(new CompanyInfo("ABC_1", 5));
        companies.add(new CompanyInfo("ABC_2", 10));
        companies.add(new CompanyInfo("ABC_3", 2));
        companies.add(new CompanyInfo("ABC_4", 6));
        companies.add(new CompanyInfo("ABC_5", 5));
        return companies;
    }
}
