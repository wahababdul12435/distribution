package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.DealerInfo;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DiscountPolicy implements Initializable {
    private int discount = 0;

    @FXML private TableView<DealerInfo> dealer_table;
    @FXML private TableColumn<DealerInfo, String> dealer_name;
    @FXML private TableColumn<DealerInfo, String> disc;
    @FXML private Slider slider_discount;
    @FXML private TextField discount_display;
    @FXML
    private Button cancelBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void initialize(URL location, ResourceBundle resources) {

        dealer_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        disc.setCellValueFactory(new PropertyValueFactory<>("totalPurchase"));

        dealer_table.getItems().setAll(parseUserList());
    }
    private List<DealerInfo> parseUserList(){
        ObservableList<DealerInfo> dealers = FXCollections.observableArrayList();
        dealers.add(new DealerInfo("Dealer_1", String.valueOf(15)+"%"));
        dealers.add(new DealerInfo("Dealer_2", String.valueOf(14)+"%"));
        dealers.add(new DealerInfo("Dealer_3", String.valueOf(10)+"%"));
        dealers.add(new DealerInfo("Dealer_4", String.valueOf(2)+"%"));
        dealers.add(new DealerInfo("Dealer_5", String.valueOf(5)+"%"));
        return dealers;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount() {
        int slid = (int) slider_discount.getValue();
        this.discount = slid;
        discount_display.setText(String.valueOf(slid)+"%");
    }
}
