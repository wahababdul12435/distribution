package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.AreaInfo;
import model.MysqlCon;
import model.ProductInfo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;


public class EnterArea {
    private int x = 1;
    @FXML
    private TextField areaID;
    @FXML
    private TextField areaName;
    @FXML private Label sub_area_1;
    @FXML private TextField sub_area_text_1;
    @FXML private Label sub_area_2;
    @FXML private TextField sub_area_text_2;
    @FXML private Label sub_area_3;
    @FXML private TextField sub_area_text_3;
    @FXML private Label sub_area_4;
    @FXML private TextField sub_area_text_4;
    @FXML private Label sub_area_5;
    @FXML private TextField sub_area_text_5;
    @FXML private Label sub_area_6;
    @FXML private TextField sub_area_text_6;
    @FXML private Label sub_area_7;
    @FXML private TextField sub_area_text_7;
    @FXML private Label sub_area_8;
    @FXML private TextField sub_area_text_8;
    @FXML
    private Button cancelBtn;

    public void addArea()
    {
        if(x == 1)
        {
            sub_area_1.setVisible(true);
            sub_area_text_1.setVisible(true);
        }
        else if(x == 2)
        {
            sub_area_2.setVisible(true);
            sub_area_text_2.setVisible(true);
        }
        else if(x == 3)
        {
            sub_area_3.setVisible(true);
            sub_area_text_3.setVisible(true);
        }
        else if(x == 4)
        {
            sub_area_4.setVisible(true);
            sub_area_text_4.setVisible(true);
        }
        else if(x == 5)
        {
            sub_area_5.setVisible(true);
            sub_area_text_5.setVisible(true);
        }
        else if(x == 6)
        {
            sub_area_6.setVisible(true);
            sub_area_text_6.setVisible(true);
        }
        else if(x == 7)
        {
            sub_area_7.setVisible(true);
            sub_area_text_7.setVisible(true);
        }
        else if(x == 8)
        {
            sub_area_8.setVisible(true);
            sub_area_text_8.setVisible(true);
        }

        x++;
    }

    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void saveClicked() throws IOException {
        String area_name = areaName.getText();
        String area_status = "Active";
        String subarea_status = "Active";
        String[] subAreas = new String[8];
        subAreas[0] = sub_area_text_1.getText();
        subAreas[1] = sub_area_text_2.getText();
        subAreas[2] = sub_area_text_3.getText();
        subAreas[3] = sub_area_text_4.getText();
        subAreas[4] = sub_area_text_5.getText();
        subAreas[5] = sub_area_text_6.getText();
        subAreas[6] = sub_area_text_7.getText();
        subAreas[7] = sub_area_text_8.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        AreaInfo objAreaInfo = new AreaInfo();
        objAreaInfo.insertArea(objStmt, objCon, area_name, area_status, subAreas, subarea_status);

        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objStage = (Stage) cancelBtn.getScene().getWindow();
        Scene objScene = new Scene(root);
        objStage.setScene(objScene);
    }
}
