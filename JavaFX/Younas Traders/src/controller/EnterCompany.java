package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.CompanyInfo;
import model.MysqlCon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

public class EnterCompany {

    private static int x = 1;
    private static int y = 1;
    @FXML
    private TextField companyName;

    @FXML
    private TextField companyAddress;

    @FXML
    private TextField companyContact;

    @FXML
    private TextField companyEmail;

    @FXML
    private Label contact_1;
    @FXML
    private TextField name_text_1;
    @FXML
    private TextField contact_text_1;
    @FXML
    private Label contact_2;
    @FXML
    private TextField name_text_2;
    @FXML
    private TextField contact_text_2;
    @FXML
    private Label contact_3;
    @FXML
    private TextField name_text_3;
    @FXML
    private TextField contact_text_3;
    @FXML
    private Label contact_4;
    @FXML
    private TextField name_text_4;
    @FXML
    private TextField contact_text_4;

    @FXML
    private Label group_1;
    @FXML
    private TextField group_text_1;
    @FXML
    private Label group_2;
    @FXML
    private TextField group_text_2;
    @FXML
    private Label group_3;
    @FXML
    private TextField group_text_3;
    @FXML
    private Label group_4;
    @FXML
    private TextField group_text_4;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button saveBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addContact()
    {
        if(x == 1)
        {
            contact_1.setVisible(true);
            name_text_1.setVisible(true);
            contact_text_1.setVisible(true);
        }
        else if(x == 2)
        {
            contact_2.setVisible(true);
            name_text_2.setVisible(true);
            contact_text_2.setVisible(true);
        }
        else if(x == 3)
        {
            contact_3.setVisible(true);
            name_text_3.setVisible(true);
            contact_text_3.setVisible(true);
        }
        else if(x == 4)
        {
            contact_4.setVisible(true);
            name_text_4.setVisible(true);
            contact_text_4.setVisible(true);
        }
        x++;
    }

    public void addGroup()
    {
        if(y == 1)
        {
            group_1.setVisible(true);
            group_text_1.setVisible(true);
        }
        else if(y == 2)
        {
            group_2.setVisible(true);
            group_text_2.setVisible(true);
        }
        else if(y == 3)
        {
            group_3.setVisible(true);
            group_text_3.setVisible(true);
        }
        else if(y == 4)
        {
            group_4.setVisible(true);
            group_text_4.setVisible(true);
        }
        y++;
        System.out.println(y);
    }

    public void saveClicked() throws IOException {
        String company_name = companyName.getText();
        String company_address = companyAddress.getText();
        String company_contact = companyContact.getText();
        String company_email = companyEmail.getText();
        String company_status = "Active";

        String[] companyAlterContact = new String[4];
        String[] companyAlterContactName = new String[4];
        String[] companyGroup = new String[4];
        if(x > 1)
        {
            companyAlterContactName[0] = name_text_1.getText();
            companyAlterContact[0] = contact_text_1.getText();
        }
        if(x > 2)
        {
            companyAlterContactName[1] = name_text_2.getText();
            companyAlterContact[1] = contact_text_2.getText();
        }
        if(x > 3)
        {
            companyAlterContactName[2] = name_text_3.getText();
            companyAlterContact[2] = contact_text_3.getText();
        }
        if(x > 4)
        {
            companyAlterContactName[3] = name_text_4.getText();
            companyAlterContact[3] = contact_text_4.getText();
        }
        

        if(y > 1)
        {
            companyGroup[0] = group_text_1.getText();
        }
        if(y > 2)
        {
            companyGroup[1] = group_text_2.getText();
        }
        if(y > 3)
        {
            companyGroup[2] = group_text_3.getText();
        }
        if(y > 4)
        {
            companyGroup[3] = group_text_4.getText();
        }

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyInfo objCompanyInfo = new CompanyInfo();
        objCompanyInfo.insertCompany(objStmt, objCon, company_name, company_address, company_contact, company_email, company_status, companyAlterContactName, companyAlterContact, companyGroup);

        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}
