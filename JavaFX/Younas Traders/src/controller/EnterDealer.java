package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.DealerInfo;
import model.MysqlCon;
import model.ProductInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

public class EnterDealer implements Initializable {
    @FXML
    private TextField dealerID;

    @FXML
    private TextField dealerName;

    @FXML
    private TextField dealerContact;

    @FXML
    private TextField dealerAddress;

    @FXML
    private ChoiceBox<String> dealerType;

    @FXML
    private TextField dealerCNIC;

    @FXML
    private TextField dealerLicNum;

    @FXML
    private TextField dealerLicExp;

    @FXML
    private ChoiceBox<String> dealerSubArea;


    @FXML private TableView<DealerInfo> dealer_table;
    @FXML private TableColumn<DealerInfo, String> dealer_name;
    @FXML private TableColumn<DealerInfo, String> total_purchase;
    @FXML
    private Button cancelBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void initialize(URL location, ResourceBundle resources) {

        dealer_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        total_purchase.setCellValueFactory(new PropertyValueFactory<>("totalPurchase"));

        dealer_table.getItems().setAll(parseUserList());
    }
    private List<DealerInfo> parseUserList(){
        ObservableList<DealerInfo> dealers = FXCollections.observableArrayList();
        dealers.add(new DealerInfo("Dealer_1", "Rs. "+"5,000"+"/-"));
        dealers.add(new DealerInfo("Dealer_2", "Rs. "+"50,000"+"/-"));
        dealers.add(new DealerInfo("Dealer_3", "Rs. "+"10,000"+"/-"));
        dealers.add(new DealerInfo("Dealer_4", "Rs. "+"34,655"+"/-"));
        dealers.add(new DealerInfo("Dealer_5", "Rs. "+"134,800"+"/-"));
        return dealers;
    }

    public void saveClicked() throws IOException {
        String dealer_areaname = dealerSubArea.getSelectionModel().getSelectedItem();
        String dealer_name = dealerName.getText();
        String dealer_contact = dealerContact.getText();
        String dealer_address = dealerAddress.getText();
        String dealer_type = dealerType.getSelectionModel().getSelectedItem();
        String dealer_cnic = dealerCNIC.getText();
        String dealer_licnum = dealerLicNum.getText();
        String dealer_licexp = dealerLicExp.getText();
        String dealer_status = "Active";

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DealerInfo objDealerInfo = new DealerInfo();
        objDealerInfo.insertDealer(objStmt, objCon, dealer_areaname, dealer_name, dealer_contact, dealer_address, dealer_type, dealer_cnic, dealer_licnum, dealer_licexp, dealer_status);

        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objStage = (Stage) cancelBtn.getScene().getWindow();
        Scene objScene = new Scene(root);
        objStage.setScene(objScene);
    }
}
