package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.CompanyInfo;
import model.MysqlCon;
import model.ProductInfo;
import model.SupplierInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

public class EnterNewProduct implements Initializable {
    @FXML
    private TextField productID;

    @FXML
    private TextField companyID;

    @FXML
    private TextField groupID;

    @FXML
    private TextField productName;

    @FXML
    private TextField retailPrice;

    @FXML
    private TextField tradePrice;

    @FXML
    private TextField purchasePrice;

    @FXML
    private TextField purchaseDiscount;

    @FXML private TableView<CompanyInfo> company_table;
    @FXML private TableColumn<CompanyInfo, String> company_name;
    @FXML private TableColumn<CompanyInfo, Integer> company_products;
    @FXML
    private Button cancelBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void initialize(URL location, ResourceBundle resources) {
        company_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        company_products.setCellValueFactory(new PropertyValueFactory<>("totalProducts"));

        company_table.getItems().setAll(parseUserList());
    }
    private List<CompanyInfo> parseUserList(){
        ObservableList<CompanyInfo> companies = FXCollections.observableArrayList();
        companies.add(new CompanyInfo("ABC_1", 50));
        companies.add(new CompanyInfo("ABC_2", 40));
        companies.add(new CompanyInfo("ABC_3", 25));
        companies.add(new CompanyInfo("ABC_4", 104));
        companies.add(new CompanyInfo("ABC_5", 134));
        return companies;
    }

    public void saveClicked() throws IOException {
        String company_id = companyID.getText();
        String group_id = groupID.getText();
        String product_name = productName.getText();
        String retail_price = retailPrice.getText();
        String trade_price = tradePrice.getText();
        String purchase_price = purchasePrice.getText();
        String purchase_discount = purchaseDiscount.getText();
        String product_status = "Active";

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ProductInfo objProductInfo = new ProductInfo();
        objProductInfo.insertProduct(objStmt, objCon, company_id, group_id, product_name, retail_price, trade_price, purchase_price, purchase_discount, product_status);

        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objStage = (Stage) cancelBtn.getScene().getWindow();
        Scene objScene = new Scene(root);
        objStage.setScene(objScene);
    }
}
