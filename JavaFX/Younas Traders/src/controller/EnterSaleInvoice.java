package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.CompanyInfo;
import model.MysqlCon;
import model.PendingOrders;
import model.SaleInvoice;




import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

public class EnterSaleInvoice {

    @FXML
    private TextField txt_dealerID;

    @FXML
    private TextField txt_areaID;

    @FXML
    private TextField txt_dealerName;

    @FXML
    private TextField txt_dealerAdd;

    @FXML
    private TextField txt_dealerLicense;

    @FXML
    private TextField txt_prodID;

    @FXML
    private TextField txt_compID;

    @FXML
    private TextField txt_prodName;

    @FXML
    private MenuBar menu_bar;

    @FXML
    private WebView viewCompany;

    @FXML
    private VBox Vbox1;

    @FXML
    private Menu pending_orders;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;

    public void keypress_AreaID() throws IOException{

        Vbox1.getChildren().add(new Button("Button " ));
        txt_compID.requestFocus();
    }

    String dealerdetail_onKeypressName[] = new String[50];
    String dealer_name = "";




    public void keypress_dealerName(KeyEvent keyEvent) throws IOException {//Function for AutoComplete
   /*     Vbox1.getChildren().clear();
        String newdealerName = txt_dealerName.getText();
        dealer_name = keyEvent.getText();
        String keeyCode = keyEvent.getCode().toString();

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                SaleInvoice objsaleinvoice = new SaleInvoice();
                dealerdetail_onKeypressName = objsaleinvoice.dealerDetail_withKeypressname(objStmt, objCon, newdealerName );
                int lengthhh = 0;
if(dealerdetail_onKeypressName[0] != null) {//if condition for no result sent back from database
    for (int i = 0; i < dealerdetail_onKeypressName.length; i++) {
        if (dealerdetail_onKeypressName[i]!=null) {
            lengthhh++;
        }
    }
    int newlength = lengthhh;
    for (int i = 0; i < newlength; i++) {
        Vbox1.getChildren().add(new TextField(dealerdetail_onKeypressName[i]));
    }
}
else {
    Vbox1.getChildren().add(new TextField("No Match Found"));
}



*/
    }

        String dealerdetails_onEnterNamebox[] = new String[1];
    public void enterpress_dealerName() throws IOException{

        String dealer_name = txt_dealerName.getText();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SaleInvoice objsaleinvoice = new SaleInvoice();
        dealerdetails_onEnterNamebox = objsaleinvoice.get_dealerDetailswithname(objStmt, objCon, dealer_name );

        String[] myArray = dealerdetails_onEnterNamebox[0].split("--");
        txt_areaID.setText(myArray[1]);
       txt_dealerID.setText(myArray[0]);
        txt_prodID.requestFocus();
    }
    String dealerdetails_onIDEnter[] = new String[1];
    public void enterpress_dealerID() throws IOException{

        String dealer_ids = txt_dealerID.getText();
        String area_ids = txt_areaID.getText();

        int intdealer_id = Integer.parseInt(dealer_ids);
        int intarea_id = Integer.parseInt(area_ids);

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SaleInvoice objsaleinvoice = new SaleInvoice();
        dealerdetails_onIDEnter = objsaleinvoice.get_dealerDetailswithIDs(objStmt, objCon,  intdealer_id, intarea_id);
        String[] myArray = dealerdetails_onIDEnter[0].split("--");
        txt_dealerName.setText(myArray[0]);
        txt_dealerAdd.setText(myArray[1]);
       txt_prodID.requestFocus();
    }

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

}
