package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.MysqlCon;
import model.ProductInfo;
import model.SalesmanInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

public class EnterSalesman implements Initializable {
    @FXML
    private TextField salesmanID;

    @FXML
    private TextField salesmanName;

    @FXML
    private TextField salesmanContact;

    @FXML
    private TextField salesmanAddress;

    @FXML
    private TextField salesmanCNIC;

    @FXML
    private ChoiceBox<String> designatedArea1;

    @FXML
    private ChoiceBox<String> designatedArea2;

    @FXML
    private ChoiceBox<String> designatedArea3;

    @FXML
    private ChoiceBox<String> designatedArea4;

    @FXML private TableView<SalesmanInfo> salesman_table;
    @FXML private TableColumn<SalesmanInfo, String> salesman_name;
    @FXML private TableColumn<SalesmanInfo, String> designated_area;
    @FXML
    private Button cancelBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void initialize(URL location, ResourceBundle resources) {
        salesman_name.setCellValueFactory(new PropertyValueFactory<>("salesmanName"));
        designated_area.setCellValueFactory(new PropertyValueFactory<>("designated_area"));

        salesman_table.getItems().setAll(parseUserList());
    }
    private List<SalesmanInfo> parseUserList(){
        ObservableList<SalesmanInfo> salesman = FXCollections.observableArrayList();
        salesman.add(new SalesmanInfo("Salesman_1", 2));
        salesman.add(new SalesmanInfo("Salesman_2", 2));
        salesman.add(new SalesmanInfo("Salesman_3", 1));
        salesman.add(new SalesmanInfo("Salesman_4", 3));
        salesman.add(new SalesmanInfo("Salesman_5", 4));
        return salesman;
    }

    public void saveClicked() throws IOException {
        String salesman_name = salesmanName.getText();
        String salesman_contact = salesmanContact.getText();
        String salesman_address = salesmanAddress.getText();
        String salesman_cnic = salesmanCNIC.getText();
        String[] designated_areas = new String[4];
        designated_areas[0] = String.valueOf(designatedArea1.getSelectionModel().getSelectedItem());
        designated_areas[1] = String.valueOf(designatedArea2.getSelectionModel().getSelectedItem());
        designated_areas[2] = String.valueOf(designatedArea3.getSelectionModel().getSelectedItem());
        designated_areas[3] = String.valueOf(designatedArea4.getSelectionModel().getSelectedItem());
//        System.out.println("Here: "+designated_areas[0]);
        String salesman_status = "Active";
        String designated_status = "Active";

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SalesmanInfo objSalesmanInfo = new SalesmanInfo();
        objSalesmanInfo.insertSalesman(objStmt, objCon, salesman_name, salesman_contact, salesman_address, salesman_cnic, salesman_status, designated_areas, designated_status);

        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objStage = (Stage) cancelBtn.getScene().getWindow();
        Scene objScene = new Scene(root);
        objStage.setScene(objScene);
    }
}
