package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.CompanyInfo;
import model.MysqlCon;
import model.SupplierInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

public class EnterSupplier implements Initializable {
    @FXML
    private TextField companyID;

    @FXML
    private TextField supplierName;

    @FXML
    private TextField supplierContact;

    @FXML
    private TextField supplierAddress;

    @FXML
    private Button saveBtn;

    @FXML private TableView<SupplierInfo> supplier_table;
    @FXML private TableColumn<SupplierInfo, String> supplier_name;
    @FXML private TableColumn<SupplierInfo, Integer> supplier_id;
    @FXML
    private Button cancelBtn;
    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void initialize(URL location, ResourceBundle resources) {
        supplier_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        supplier_id.setCellValueFactory(new PropertyValueFactory<>("id"));

        supplier_table.getItems().setAll(parseUserList());
    }
    private List<SupplierInfo> parseUserList(){
        ObservableList<SupplierInfo> suppliers = FXCollections.observableArrayList();
        suppliers.add(new SupplierInfo(1, "ABC_1"));
        suppliers.add(new SupplierInfo(2, "ABC_2"));
        suppliers.add(new SupplierInfo(3, "ABC_3"));
        suppliers.add(new SupplierInfo(4, "ABC_4"));
        suppliers.add(new SupplierInfo(5, "ABC_5"));
        return suppliers;
    }

    public void saveClicked() throws IOException {
        String company_id = companyID.getText();
        String supplier_name = supplierName.getText();
        String supplier_contact = supplierContact.getText();
        String supplier_address = supplierAddress.getText();
        String supplier_status = "Active";

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SupplierInfo objSupplierInfo = new SupplierInfo();
        objSupplierInfo.insertSupplier(objStmt, objCon, company_id, supplier_name, supplier_contact, supplier_address, supplier_status);

        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objStage = (Stage) cancelBtn.getScene().getWindow();
        Scene objScene = new Scene(root);
        objStage.setScene(objScene);
    }
}
