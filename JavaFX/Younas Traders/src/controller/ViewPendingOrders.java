package controller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.MysqlCon;
import model.PendingOrders;
import netscape.javascript.JSObject;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ViewPendingOrders implements Initializable {
    @FXML
    private WebView viewPending;

    @FXML
    private MenuBar menu_bar;

    @FXML
    private Menu pending_orders;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        unSeenOrders = 0;
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objPendingOrders = new PendingOrders();
        unSeenOrders = objPendingOrders.getUnSeenOrders(objStmt, objCon);
        pending_orders.setText("Pending Orders (" + unSeenOrders + ")");

        WebEngine objEngine = viewPending.getEngine();

        // process page loading
        objEngine.getLoadWorker().stateProperty().addListener(
                new ChangeListener<Worker.State>() {
                    @Override
                    public void changed(ObservableValue<? extends Worker.State> ov,
                                        Worker.State oldState, Worker.State newState) {
                        if (newState == Worker.State.SUCCEEDED) {
                            JSObject win = (JSObject) objEngine.executeScript("window");
                            win.setMember("app", new JavaApp());
                        }
                    }
                }
        );

        objEngine.load("http://localhost/YounasTraders/ViewPendingOrders.php");
    }

    // JavaScript interface object
    public class JavaApp {

        public void exit(String id) {
            System.out.println(id);
        }
    }

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

}
