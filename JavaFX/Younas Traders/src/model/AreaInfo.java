package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AreaInfo {
    private int id;
    private String name;

    public AreaInfo()
    {
        this.id = 0;
        this.name = "";
    }

    public AreaInfo(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void insertArea(Statement stmt, Connection con, String areaName, String areaStatus, String[] subAreas, String subAreaStatus)
    {
        try {
            String getAreaID = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'younas_traders' AND TABLE_NAME = 'area_info'";
            ResultSet rs =stmt.executeQuery(getAreaID);
            rs.next();
            int areaID = rs.getInt(1);

            String insertQuery = "INSERT INTO `area_info`(`area_name`, `area_status`) VALUES ('"+areaName+"','"+areaStatus+"')";
            stmt.executeUpdate(insertQuery);

            for(int x=0; x<subAreas.length; x++)
            {
                String insertSubAreas = "INSERT INTO `subarea_info`(`area_id`, `sub_area_name`, `subarea_status`) VALUES ('"+areaID+"','"+subAreas[x]+"','"+subAreaStatus+"')";
                stmt.executeUpdate(insertSubAreas);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] viewArea(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
