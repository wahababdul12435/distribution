package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CompanyInfo {
    private int id;
    private String name;
    private int totalProducts;

    public CompanyInfo()
    {
        id = 0;
        name = "";
    }

    public CompanyInfo(int companyID, String companyName)
    {
        this.id = companyID;
        this.name = companyName;
    }

    public CompanyInfo(String companyName, int products)
    {
        this.totalProducts = products;
        this.name = companyName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(int totalProducts) {
        this.totalProducts = totalProducts;
    }

    public void insertCompany(Statement stmt, Connection con, String companyName, String companyAddress, String companyContact, String companyEmail, String companyStatus, String[] companyAlterContactName, String[] companyAlterContact, String[] companyGroup)
    {
        try {
            String insertQuery = "INSERT INTO `company_info`(`company_name`, `company_address`, `company_contact`, `company_email`, `company_status`) VALUES ('"+companyName+"','"+companyAddress+"','"+companyContact+"','"+companyEmail+"','"+companyStatus+"')";
//            System.out.println(insertQuery);
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] viewCompanies(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
