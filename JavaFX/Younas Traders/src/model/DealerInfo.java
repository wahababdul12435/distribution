package model;

import javafx.fxml.FXML;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DealerInfo {
    private int id;
    private String name;
    private String totalPurchase;

    public DealerInfo()
    {
        id = 0;
        name = "";
    }

    public DealerInfo(int dealerID, String dealerName)
    {
        this.id = dealerID;
        this.name = dealerName;
    }

    public DealerInfo(String dealerName, String purchase)
    {
        this.totalPurchase = purchase;
        this.name = dealerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public void insertDealer(Statement stmt, Connection con, String dealerAreaName, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCNIC, String dealerLicNum, String dealerLicExp, String dealerStatus)
    {
        try {
            String getSubAreaID = "SELECT `subarea_id` FROM `subarea_info` WHERE `sub_area_name` = '"+dealerAreaName+"'";
            ResultSet rs =stmt.executeQuery(getSubAreaID);
            rs.next();
            String dealerAreaID = rs.getString(1);

            String insertQuery = "INSERT INTO `dealer_info`(`dealer_area_id`, `dealer_name`, `dealer_contact`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic_num`, `dealer_lic_exp`, `dealer_status`) VALUES ('"+dealerAreaID+"','"+dealerName+"','"+dealerContact+"','"+dealerAddress+"','"+dealerType+"','"+dealerCNIC+"','"+dealerLicNum+"','"+dealerLicExp+"','"+dealerStatus+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] viewDealer(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
