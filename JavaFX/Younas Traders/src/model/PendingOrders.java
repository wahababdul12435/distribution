package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class PendingOrders {
    int unSeenOrders;

    public PendingOrders()
    {
        this.unSeenOrders = 0;
    }

    public PendingOrders(int unSeenOrders)
    {
        this.unSeenOrders = unSeenOrders;
    }

    public int getUnSeenOrders(Statement stmt, Connection con)
    {
        try {
            String getPendingOrders = "SELECT COUNT(`status`) FROM `order_info` WHERE `status` = 'Pending'";
            ResultSet rs =stmt.executeQuery(getPendingOrders);
            rs.next();
            unSeenOrders = Integer.valueOf(rs.getString(1));
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return unSeenOrders;
    }
}
