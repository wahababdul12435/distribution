package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProductInfo {
    private int id;
    private String name;

    public ProductInfo()
    {
        this.id = 0;
        this.name = "";
    }

    public ProductInfo(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void insertProduct(Statement stmt, Connection con, String companyID, String groupID, String productName, String retailPrice, String tradePrice, String purchasePrice, String purchaseDiscount, String productStatus)
    {
        try {
            String insertQuery = "INSERT INTO `product_info`(`company_id`, `group_id`, `product_name`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `product_status`) VALUES ('"+companyID+"','"+groupID+"','"+productName+"','"+retailPrice+"','"+tradePrice+"','"+purchasePrice+"','"+purchaseDiscount+"','"+productStatus+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] viewProduct(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
