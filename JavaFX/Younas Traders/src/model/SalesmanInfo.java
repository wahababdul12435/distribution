package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SalesmanInfo {
    private String salesmanName;
    private int designated_area;

    public SalesmanInfo()
    {
        this.salesmanName = "";
        this.designated_area = 0;
    }
    public SalesmanInfo(String name, int area)
    {
        this.salesmanName = name;
        this.designated_area = area;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public int getDesignated_area() {
        return designated_area;
    }

    public void setDesignated_area(int designated_area) {
        this.designated_area = designated_area;
    }

    public void insertSalesman(Statement stmt, Connection con, String salesmanName, String salesmanContact, String salesmanAddress, String salesmanCNIC, String salesmanStatus, String designatedAreas[], String designatedStatus)
    {
        try {
            String getSalesmanID = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'younas_traders' AND TABLE_NAME = 'salesman_info'";
            ResultSet rs =stmt.executeQuery(getSalesmanID);
            rs.next();
            int salesmanID = rs.getInt(1);

            String designatedAreasID[] = new String[4];
            for(int x=0; x<designatedAreas.length; x++)
            {
                String getSubAreaID = "SELECT `subarea_id` FROM `subarea_info` WHERE `sub_area_name` = '"+designatedAreas[x]+"'";
                rs =stmt.executeQuery(getSubAreaID);
                rs.next();
                designatedAreasID[x] = rs.getString(1);
            }

            String insertQuery = "INSERT INTO `salesman_info`(`salesman_name`, `salesman_contact`, `salesman_address`, `salesman_cnic`, `salesman_status`) VALUES ('"+salesmanName+"','"+salesmanContact+"','"+salesmanAddress+"','"+salesmanCNIC+"','"+salesmanStatus+"')";
            stmt.executeUpdate(insertQuery);

            for(int x=0; x<designatedAreasID.length; x++)
            {
                String insertAreas = "INSERT INTO `salesman_designated_areas`(`salesman_id`, `designated_area_id`, `designated_status`) VALUES ('"+salesmanID+"','"+designatedAreasID[x]+"','"+designatedStatus+"')";
                stmt.executeUpdate(insertAreas);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] viewProduct(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
