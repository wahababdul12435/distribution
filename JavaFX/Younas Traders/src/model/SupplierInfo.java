package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SupplierInfo {

    private int id;
    private String name;

    public SupplierInfo()
    {
        this.id = 0;
        this.name = "";
    }

    public SupplierInfo(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void insertSupplier(Statement stmt, Connection con, String companyID, String supplierName, String supplierContact, String supplierAddress, String supplierStatus)
    {
        try {
            String insertQuery = "INSERT INTO `supplier_info`(`company_id`, `supplier_name`, `supplier_contact`, `supplier_address`, `supplier_status`) VALUES ('"+companyID+"','"+supplierName+"','"+supplierContact+"','"+supplierAddress+"','"+supplierStatus+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] viewSupplier(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
