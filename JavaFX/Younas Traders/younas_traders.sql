-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 21, 2019 at 11:52 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `younas_traders`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_info`
--

DROP TABLE IF EXISTS `area_info`;
CREATE TABLE IF NOT EXISTS `area_info` (
  `area_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_name` varchar(250) NOT NULL,
  `area_status` varchar(50) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_alternate_contacts`
--

DROP TABLE IF EXISTS `company_alternate_contacts`;
CREATE TABLE IF NOT EXISTS `company_alternate_contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `contact_name` varchar(150) NOT NULL,
  `contact_number` varchar(60) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_groups`
--

DROP TABLE IF EXISTS `company_groups`;
CREATE TABLE IF NOT EXISTS `company_groups` (
  `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
CREATE TABLE IF NOT EXISTS `company_info` (
  `company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(300) NOT NULL,
  `company_contact` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_status` varchar(50) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`company_id`, `company_name`, `company_address`, `company_contact`, `company_email`, `company_status`) VALUES
(1, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Company Status 1'),
(2, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(3, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_info`
--

DROP TABLE IF EXISTS `dealer_info`;
CREATE TABLE IF NOT EXISTS `dealer_info` (
  `dealer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_area_id` int(10) UNSIGNED NOT NULL,
  `dealer_name` varchar(200) NOT NULL,
  `dealer_contact` varchar(100) NOT NULL,
  `dealer_address` varchar(250) NOT NULL,
  `dealer_type` varchar(30) NOT NULL,
  `dealer_cnic` varchar(30) NOT NULL,
  `dealer_lic_num` varchar(80) NOT NULL,
  `dealer_lic_exp` varchar(50) NOT NULL,
  `dealer_status` varchar(50) NOT NULL,
  PRIMARY KEY (`dealer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
CREATE TABLE IF NOT EXISTS `product_info` (
  `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `purchase_discount` float NOT NULL,
  `product_status` varchar(50) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_designated_areas`
--

DROP TABLE IF EXISTS `salesman_designated_areas`;
CREATE TABLE IF NOT EXISTS `salesman_designated_areas` (
  `salesman_designated_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) UNSIGNED NOT NULL,
  `designated_area_1_id` int(10) UNSIGNED NOT NULL,
  `designated_area_2_id` int(10) UNSIGNED NOT NULL,
  `designated_area_3_id` int(10) UNSIGNED NOT NULL,
  `designated_area_4_id` int(10) UNSIGNED NOT NULL,
  `designated_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_designated_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_info`
--

DROP TABLE IF EXISTS `salesman_info`;
CREATE TABLE IF NOT EXISTS `salesman_info` (
  `salesman_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_name` varchar(200) NOT NULL,
  `salesman_contact` varchar(60) NOT NULL,
  `salesman_address` varchar(250) NOT NULL,
  `salesman_cnic` varchar(50) NOT NULL,
  `salesman_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subarea_info`
--

DROP TABLE IF EXISTS `subarea_info`;
CREATE TABLE IF NOT EXISTS `subarea_info` (
  `subarea_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_id` int(10) UNSIGNED NOT NULL,
  `sub_area_name_1` varchar(250) NOT NULL,
  `sub_area_name_2` varchar(250) NOT NULL,
  `sub_area_name_3` varchar(250) NOT NULL,
  `sub_area_name_4` varchar(250) NOT NULL,
  `sub_area_name_5` varchar(250) NOT NULL,
  `sub_area_name_6` varchar(250) NOT NULL,
  `sub_area_name_7` varchar(250) NOT NULL,
  `sub_area_name_8` varchar(250) NOT NULL,
  `subarea_status` varchar(50) NOT NULL,
  PRIMARY KEY (`subarea_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

DROP TABLE IF EXISTS `supplier_info`;
CREATE TABLE IF NOT EXISTS `supplier_info` (
  `supplier_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(60) NOT NULL,
  `supplier_address` varchar(200) NOT NULL,
  `supplier_status` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
