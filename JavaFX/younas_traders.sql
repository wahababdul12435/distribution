-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 06, 2020 at 03:36 PM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `younas_traders`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_info`
--

DROP TABLE IF EXISTS `area_info`;
CREATE TABLE IF NOT EXISTS `area_info` (
  `area_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_name` varchar(250) NOT NULL,
  `area_status` varchar(50) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_info`
--

INSERT INTO `area_info` (`area_id`, `area_name`, `area_status`) VALUES
(1, 'Gujrat Center', 'Active'),
(2, 'Main Area', 'Active'),
(4, 'Gujrat Center 1 Test', 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `company_alternate_contacts`
--

DROP TABLE IF EXISTS `company_alternate_contacts`;
CREATE TABLE IF NOT EXISTS `company_alternate_contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `contact_name` varchar(150) NOT NULL,
  `contact_number` varchar(60) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_groups`
--

DROP TABLE IF EXISTS `company_groups`;
CREATE TABLE IF NOT EXISTS `company_groups` (
  `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_groups`
--

INSERT INTO `company_groups` (`group_id`, `company_id`, `group_name`) VALUES
(1, 2, 'Group 2 1'),
(2, 2, 'Group 2 2'),
(3, 3, 'Group 3 1'),
(4, 3, 'Group 3 2'),
(5, 2, 'Group 2 3');

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
CREATE TABLE IF NOT EXISTS `company_info` (
  `company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(300) NOT NULL,
  `company_contact` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_status` varchar(50) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=339 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`company_id`, `company_name`, `company_address`, `company_contact`, `company_email`, `company_status`) VALUES
(1, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(2, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(3, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(11, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(5, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(6, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(7, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(8, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(9, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(12, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(13, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(14, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(15, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(16, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(17, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(18, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(19, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(20, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(21, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(22, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(23, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(24, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(25, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(26, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(27, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(28, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(29, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(30, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(31, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(32, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(33, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(34, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(35, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(36, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(37, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(38, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(39, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(40, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(41, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(42, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(43, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(44, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(45, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(46, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(47, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(48, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(49, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(50, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(51, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(52, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(53, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(54, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(55, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(56, 'Company Name 6', 'Company Address 6 Test of 56', 'Company Contact 6', 'Company Email 6', 'Active'),
(57, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(58, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(59, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(60, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(61, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(62, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(63, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(64, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(65, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(66, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(67, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(68, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(69, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(70, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(71, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(72, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(73, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(74, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(75, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(76, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(77, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(78, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(79, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(80, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(81, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(82, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(83, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(84, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(85, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(86, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(87, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(88, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(89, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(90, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(91, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(92, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(93, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(94, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(95, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(96, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(97, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(98, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(99, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(100, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(101, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(102, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(103, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(104, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(105, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(106, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(107, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(108, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(109, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(110, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(111, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(112, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(113, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(114, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(115, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(116, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(117, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(118, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(119, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(120, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(121, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(122, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(123, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(124, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(125, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(126, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(127, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(128, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(129, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(130, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(131, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(132, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(133, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(134, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(135, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(136, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(137, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(138, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(139, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(140, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(141, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(142, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(143, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(144, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(145, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(146, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(147, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(148, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(149, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(150, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(151, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(152, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(153, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(154, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(155, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(156, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(157, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(158, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(159, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(160, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(161, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(162, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(163, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(164, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(165, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(166, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(167, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(168, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(169, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(170, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(171, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(172, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(173, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(174, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(175, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(176, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(177, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(178, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(179, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(180, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(181, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(182, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(183, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(184, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(185, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(186, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(187, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(188, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(189, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(190, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(191, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(192, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(193, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(194, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(195, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(196, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(197, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(198, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(199, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(200, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(201, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(202, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(203, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(204, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(205, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(206, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(207, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(208, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(209, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(210, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(211, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(212, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(213, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(214, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(215, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(216, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(217, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(218, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(219, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(220, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(221, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(222, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(223, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(224, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(225, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(226, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(227, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(228, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(229, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(230, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(231, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(232, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(233, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(234, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(235, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(236, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(237, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(238, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(239, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(240, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(241, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(242, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(243, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(244, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(245, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(246, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(247, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(248, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(249, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(250, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(251, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(252, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', ''),
(253, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(254, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(255, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(256, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(257, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(258, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(259, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(260, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(261, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(262, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(263, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(264, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(265, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(266, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(267, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(268, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(269, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(270, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(271, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(272, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(273, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(274, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(275, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(276, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(277, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(278, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(279, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(280, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(281, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(282, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(283, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(284, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(285, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(286, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(287, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(288, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(289, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(290, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(291, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(292, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(293, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(294, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(295, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(296, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(297, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(298, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(299, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(300, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(301, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(302, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(303, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(304, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(305, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(306, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(307, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(308, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(309, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(310, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(311, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(312, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(313, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(314, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(315, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(316, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(317, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(318, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(319, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(320, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(321, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(322, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(323, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(324, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(325, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(326, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(327, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(328, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(329, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(330, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(331, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(332, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(333, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(334, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(335, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(336, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(337, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(338, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', '');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_info`
--

DROP TABLE IF EXISTS `dealer_info`;
CREATE TABLE IF NOT EXISTS `dealer_info` (
  `dealer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_area_id` int(10) UNSIGNED NOT NULL,
  `dealer_name` varchar(200) NOT NULL,
  `dealer_contact` varchar(100) NOT NULL,
  `dealer_address` varchar(250) NOT NULL,
  `dealer_type` varchar(30) NOT NULL,
  `dealer_cnic` varchar(30) NOT NULL,
  `dealer_lic_num` varchar(80) NOT NULL,
  `dealer_lic_exp` varchar(50) NOT NULL,
  `dealer_status` varchar(50) NOT NULL,
  PRIMARY KEY (`dealer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_info`
--

INSERT INTO `dealer_info` (`dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic_num`, `dealer_lic_exp`, `dealer_status`) VALUES
(1, 2, 'Dealer Name 1 Test', 'Dealer Contact 1 Test', 'Dealer Address 1 Test', 'Retailer', 'Dealer CNIC 1 Test', 'Dealer Lic Num 1 Test', 'Dealer Lic Exp 1 Test', 'In Active'),
(2, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active'),
(3, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active'),
(4, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `order_info`
--

DROP TABLE IF EXISTS `order_info`;
CREATE TABLE IF NOT EXISTS `order_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) DEFAULT NULL,
  `product_id` varchar(500) DEFAULT NULL,
  `quantity` varchar(150) DEFAULT NULL,
  `unit` varchar(50) NOT NULL,
  `order_price` float NOT NULL,
  `bonus` varchar(150) NOT NULL,
  `discount` float NOT NULL,
  `total_price` float NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `salesman_id` int(10) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_info`
--

INSERT INTO `order_info` (`id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `total_price`, `date`, `time`, `salesman_id`, `status`) VALUES
(1, 12, '123', '5', 'Box', 0, '0', 0, 0, '01/Oct/2019', '09:58 PM', 1, 'Pending'),
(2, 123, '123_-_456', '5_-_10', 'Box_-_Packets', 0, '0', 0, 0, '10/Nov/2019', '09:58 PM', 1, 'Pending'),
(3, 123, '456', '10', 'Packets', 0, '0', 0, 0, '10/Nov/2019', '10:01 PM', 2, 'Pending'),
(4, 1, '1', '1', 'Box', 0, '0', 0, 0, '11/Nov/2019', '12:05 AM', 2, 'Pending'),
(5, 2, '2', '2', 'Box', 0, '0', 0, 0, '11/Nov/2019', '12:46 AM', 1, 'Pending'),
(6, 2, '2', '2', 'Box', 0, '0', 0, 0, '11/Nov/2019', '01:08 AM', 4, 'Pending'),
(7, 4, '4', '4', 'Packets', 0, '0', 0, 0, '11/Nov/2019', '02:09 AM', 1, 'Pending'),
(8, 5, '5', '5', 'Box', 0, '0', 0, 0, '11/Nov/2019', '02:15 AM', 3, 'Pending'),
(9, 6, '6', '6', 'Box', 0, '0', 0, 0, '11/Nov/2019', '02:17 AM', 0, 'Pending'),
(10, 7, '7', '7', 'Box', 0, '0', 0, 0, '11/Nov/2019', '02:19 AM', 0, 'Pending'),
(11, 5, '5', '5', 'Packets', 0, '0', 0, 0, '11/Nov/2019', '03:53 AM', 0, 'Pending'),
(12, 4, '4', '4', 'Box', 0, '0', 0, 0, '11/Nov/2019', '09:11 AM', 0, 'Pending'),
(13, 5, '5', '5', 'Box', 0, '0', 0, 0, '11/Nov/2019', '09:11 AM', 0, 'Pending'),
(14, 5, '5', '5', 'Box', 0, '0', 0, 0, '11/Nov/2019', '09:12 AM', 0, 'Pending'),
(15, 4, '4', '4', 'Box', 0, '0', 0, 0, '11/Nov/2019', '09:15 AM', 0, 'Pending'),
(16, 2, '2', '2', 'Box', 0, '0', 0, 0, '11/Nov/2019', '09:39 AM', 0, 'Pending'),
(17, 3, '3', '3', 'Box', 0, '0', 0, 0, '11/Nov/2019', '09:40 AM', 0, 'Pending'),
(18, 4, '4', '4', 'Box', 0, '0', 0, 0, '17/Nov/2019', '12:34 AM', 0, 'Pending'),
(19, 4441, '4441', '4441', 'Packets', 0, '0', 0, 0, '17/Nov/2019', '12:45 AM', 0, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
CREATE TABLE IF NOT EXISTS `product_info` (
  `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `purchase_discount` float NOT NULL,
  `product_status` varchar(50) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=457 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_id`, `company_id`, `group_id`, `product_name`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `product_status`) VALUES
(1, 3, 3, 'Name 1', 34, 32, 43, 2, 'Active'),
(2, 3, 4, 'Name 1', 34, 32, 43, 2, 'Active'),
(3, 2, 2, 'Name 3333', 3400, 3200, 4300, 20, 'Active'),
(123, 3, 4, 'Name 123', 34, 32, 43, 2, 'Active'),
(456, 3, 4, 'Name 456', 34, 32, 43, 2, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `salesman_designated_areas`
--

DROP TABLE IF EXISTS `salesman_designated_areas`;
CREATE TABLE IF NOT EXISTS `salesman_designated_areas` (
  `salesman_designated_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) UNSIGNED NOT NULL,
  `designated_area_id` int(10) UNSIGNED NOT NULL,
  `designated_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_designated_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesman_designated_areas`
--

INSERT INTO `salesman_designated_areas` (`salesman_designated_id`, `salesman_id`, `designated_area_id`, `designated_status`) VALUES
(1, 2, 3, 'Active'),
(2, 3, 3, 'Active'),
(3, 3, 2, 'Active'),
(6, 4, 3, 'In Active'),
(7, 4, 2, 'In Active'),
(8, 4, 3, 'In Active'),
(9, 4, 2, 'In Active'),
(10, 1, 3, 'In Active'),
(11, 1, 5, 'In Active'),
(12, 2, 5, 'Active'),
(13, 1, 6, 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `salesman_info`
--

DROP TABLE IF EXISTS `salesman_info`;
CREATE TABLE IF NOT EXISTS `salesman_info` (
  `salesman_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_name` varchar(200) NOT NULL,
  `salesman_contact` varchar(60) NOT NULL,
  `salesman_address` varchar(250) NOT NULL,
  `salesman_cnic` varchar(50) NOT NULL,
  `salesman_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesman_info`
--

INSERT INTO `salesman_info` (`salesman_id`, `salesman_name`, `salesman_contact`, `salesman_address`, `salesman_cnic`, `salesman_status`) VALUES
(1, 'Salesman Name 1 Test', 'Salesman Contact 1 Test', 'Salesman Address 1 Test', 'Salesman CNIC 1 Test', 'In Active'),
(2, 'wdwqd', 'dwdqwwd', 'wdqwd', 'dwq', 'Active'),
(3, 'dwdw', 'swqdwq', 'dwqd', 'eweqwd', 'Active'),
(4, 'Salesman 4', 'Salesman 4', 'Salesman 4', 'Salesman 4', 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `subarea_info`
--

DROP TABLE IF EXISTS `subarea_info`;
CREATE TABLE IF NOT EXISTS `subarea_info` (
  `subarea_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_id` int(10) UNSIGNED NOT NULL,
  `sub_area_name` varchar(250) NOT NULL,
  `subarea_status` varchar(50) NOT NULL,
  PRIMARY KEY (`subarea_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subarea_info`
--

INSERT INTO `subarea_info` (`subarea_id`, `area_id`, `sub_area_name`, `subarea_status`) VALUES
(2, 1, 'Area ABC', 'Active'),
(3, 1, 'Area XYZ', 'Active'),
(4, 2, 'Sub 1', 'Active'),
(5, 2, 'Sub 2', 'Active'),
(6, 2, 'Sub 3', 'Active'),
(7, 2, 'Sub 4', 'Active'),
(8, 2, 'Sub 5', 'Active'),
(9, 2, 'Sub 6', 'Active'),
(10, 2, 'Sub 7 Test', 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

DROP TABLE IF EXISTS `supplier_info`;
CREATE TABLE IF NOT EXISTS `supplier_info` (
  `supplier_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(60) NOT NULL,
  `supplier_address` varchar(200) NOT NULL,
  `supplier_status` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_info`
--

INSERT INTO `supplier_info` (`supplier_id`, `company_id`, `supplier_name`, `supplier_contact`, `supplier_address`, `supplier_status`) VALUES
(1, 2, 'Supplier Name 1', 'Supplier Contact 1', 'Supplier Address 1', 'Active'),
(2, 2, 'Supplier Name 1', 'Supplier Contact 1', 'Supplier Address 1', 'Active'),
(4, 2, 'Supplier Name 1', 'Supplier Contact 1', 'Supplier Address 1', 'Active'),
(5, 1, 'abc Name', '243432', 'abc address', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(300) NOT NULL,
  `user_power` varchar(50) NOT NULL,
  `reg_date` varchar(50) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
