<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 18-Jan-20
 * Time: 1:15 PM
 */

require_once "../connection.php";

if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
}

$data = array();

$tableName = $_GET['tablename'];
if($tableName == 'subarea_info')
{
    $sql = "SELECT * FROM `subarea_info`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $areaId, $subareaName, $subareaStatus);
    while($stmt->fetch()){
        $temp = [
            'id'=>$id,
            'areaid'=>$areaId,
            'subarea_name'=>$subareaName,
            'subarea_status'=>$subareaStatus
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'dealer_info')
{
    $sql = "SELECT * FROM `dealer_info`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $areaId, $dealerName, $dealerContact, $dealerAddress, $dealerType, $dealerCnic, $dealerLicNum, $dealerLicExp, $dealerStatus);
    while($stmt->fetch()){
        $temp = [
            'id'=>$id,
            'areaid'=>$areaId,
            'dealer_name'=>$dealerName,
            'dealer_contact'=>$dealerContact,
            'dealer_address'=>$dealerAddress,
            'dealer_type'=>$dealerType,
            'dealer_cnic'=>$dealerCnic,
            'dealer_licnum'=>$dealerLicNum,
            'dealer_licexp'=>$dealerLicExp,
            'dealer_status'=>$dealerStatus
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'product_info')
{
    $sql = "SELECT `product_id`, `product_name`, `final_price`, `product_status` FROM `product_info`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $productName, $finalPrice, $productStatus);
    while($stmt->fetch()){
        $temp = [
            'id'=>$id,
            'product_name'=>$productName,
            'final_price'=>$finalPrice,
            'product_status'=>$productStatus
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($tableName == 'user_accounts')
{
    $userId = "";
}

//$heroes = array();
//$sql = "SELECT subarea_id, sub_area_name FROM subarea_info;";
//$stmt = $con->prepare($sql);
//$stmt->execute();
//$stmt->bind_result($id, $name);
//while($stmt->fetch()){
//    $temp = [
//        'id'=>$id,
//        'name'=>$name
//    ];
//    array_push($heroes, $temp);
//}
//echo json_encode($heroes);

?>