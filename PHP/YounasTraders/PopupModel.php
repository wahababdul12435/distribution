<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 30-Oct-19
 * Time: 1:44 AM
 */
?>

<style>
    table {
        border-collapse: collapse;
    }

    td {
        position: relative;
        padding: 5px 10px;
    }

    tr.strikeout td:before {
        content: " ";
        position: absolute;
        top: 50%;
        left: 0;
        border-bottom: 1px solid #111;
        width: 100%;
    }
</style>

<!-- Trigger the modal with a button -->
<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->
<!-- Modal -->
<div id="myModalDel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure, you want to delete company record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="delConfirmation('yes')" data-dismiss="modal">Yes</button>
                <button type="button" class="btn btn-default" onclick="delConfirmation('no')" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>

<div id="myModalEditCompany" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Company Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Company ID
                        <input type="text" id="com_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Company Name
                        <input type="text" id="com_name" class="form-control" value=""><br>
                        Company Address
                        <input type="text" id="com_address" class="form-control" value=""><br>
                        Company Contact
                        <input type="text" id="com_contact" class="form-control" value=""><br>
                        Company Email
                        <input type="text" id="com_email" class="form-control" value=""><br>
                        Company Status
                        <select id="com_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" name="update_company" onclick="updateData('yes')" class="btn btn-primary" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditSupplier" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Supplier Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Supplier ID
                        <input type="text" id="sup_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
<!--                        <input type="hidden" id="sup_companyId" class="form-control" value="">-->
                        Company Name
                        <select id="sup_companyname" class="form-control">
                            <?php
                            for($x=0; $x<$comCount; $x++)
                            {
                                ?>
                            <option value="<?php echo $comIDMain[$x];?>"><?php echo $comNameMain[$x]; ?></option>
                            <?php
                            }
                            ?>
                        </select><br>
                        Supplier Name
                        <input type="text" id="sup_name" class="form-control" value=""><br>
                        Supplier Contact
                        <input type="text" id="sup_contact" class="form-control" value=""><br>
                        Supplier Address
                        <input type="text" id="sup_address" class="form-control" value=""><br>
                        Supplier Status
                        <select id="sup_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditProduct" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Product Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Product ID
                        <input type="text" id="pro_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Company Name
                        <select id="pro_companyid" class="form-control" onchange="changeCompany()">
                            <?php
                            for($x=0; $x<$companyCount; $x++)
                            {
                                ?>
                                <option value="<?php echo $companyIDMain[$x];?>"><?php echo $companyNameMain[$x]; ?></option>
                                <?php
                            }
                            ?>
                        </select><br>
                        Group Name
                        <?php
                        for($x=0; $x<$companyCount; $x++)
                        {
                            if(in_array($companyIDMain[$x], $companyIDGroupMain))
                            {
                                ?>
                                <select id="<?php echo 'pro_groupname_'.$companyIDMain[$x]?>" class="form-control" style="display: none">
                                    <?php
                                    $arrIndexes = array_values_index($companyIDMain[$x], $companyIDGroupMain);
                                    for($y=0; $y<sizeof($arrIndexes); $y++)
                                    {
                                        ?>
                                        <option value="<?php echo $groupIDMain[$arrIndexes[$y]];?>"><?php echo $groupNameMain[$arrIndexes[$y]]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                            }
                            else
                            {
                                ?>
                                <select id="<?php echo 'pro_groupname_'.$companyIDMain[$x]?>" class="form-control" style="display: none">
                                    <option value="No Group">No Group</option>
                                </select>
                                <?php
                            }
                        }
                        ?><br>
                        Product Name
                        <input type="text" id="pro_name" class="form-control" value=""><br>
                        Retail Price
                        <input type="text" id="pro_retailprice" class="form-control" value=""><br>
                        Trade Price
                        <input type="text" id="pro_tradeprice" class="form-control" value=""><br>
                        Purchase Price
                        <input type="text" id="pro_purchaseprice" class="form-control" value=""><br>
                        Purchase Discount
                        <input type="text" id="pro_purchasediscount" class="form-control" value=""><br>
                        Product Status
                        <select id="pro_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" onclick="updateData('no')" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditSalesman" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Salesman Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Salesman ID
                        <input type="text" id="salesman_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Salesman Name
                        <input type="text" id="salesman_name" class="form-control" value=""><br>
                        Salesman Contact
                        <input type="text" id="salesman_contact" class="form-control" value=""><br>
                        Salesman Address
                        <input type="text" id="salesman_address" class="form-control" value=""><br>
                        Salesman CNIC
                        <input type="text" id="salesman_cnic" class="form-control" value=""><br>
                        Designated Area 1
                        <select id="salesman_des_1" class="form-control">
                            <?php
                            for($x=0; $x<$areasCount; $x++)
                            {
                                ?>
                            <option value="<?php echo $areaID[$x]; ?>"><?php echo $areaName[$x]; ?></option>
                            <?php
                            }
                            ?>
                        </select><br>
                        Designated Area 2
                        <select id="salesman_des_2" class="form-control">
                            <?php
                            for($x=0; $x<$areasCount; $x++)
                            {
                                ?>
                                <option value="<?php echo $areaID[$x]; ?>"><?php echo $areaName[$x]; ?></option>
                                <?php
                            }
                            ?>
                        </select><br>
                        Designated Area 3
                        <select id="salesman_des_3" class="form-control">
                            <?php
                            for($x=0; $x<$areasCount; $x++)
                            {
                                ?>
                                <option value="<?php echo $areaID[$x]; ?>"><?php echo $areaName[$x]; ?></option>
                                <?php
                            }
                            ?>
                        </select><br>
                        Designated Area 4
                        <select id="salesman_des_4" class="form-control">
                            <?php
                            for($x=0; $x<$areasCount; $x++)
                            {
                                ?>
                                <option value="<?php echo $areaID[$x]; ?>"><?php echo $areaName[$x]; ?></option>
                                <?php
                            }
                            ?>
                        </select><br>
                        Salesman Status
                        <select id="salesman_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" onclick="updateData('no')" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditDealer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Dealer Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Dealer ID
                        <input type="text" id="dealer_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Area Name
                        <select id="dealer_areaname" class="form-control">
                            <?php
                            for($x=0; $x<$areasCount; $x++)
                            {
                                ?>
                                <option value="<?php echo $areaID[$x]; ?>"><?php echo $areaName[$x]; ?></option>
                                <?php
                            }
                            ?>
                        </select><br>
                        Dealer Name
                        <input type="text" id="dealer_name" class="form-control" value=""><br>
                        Dealer Contact
                        <input type="text" id="dealer_contact" class="form-control" value=""><br>
                        Dealer Address
                        <input type="text" id="dealer_address" class="form-control" value=""><br>
                        Dealer Type
                        <select id="dealer_type" class="form-control">
                            <option value="Doctor">Doctor</option>
                            <option value="Retailer">Retailer</option>
                        </select><br>
                        Dealer CNIC
                        <input type="text" id="dealer_cnic" class="form-control" value=""><br>
                        Dealer Lic Num
                        <input type="text" id="dealer_licnum" class="form-control" value=""><br>
                        Dealer Lic Exp
                        <input type="text" id="dealer_licexp" class="form-control" value=""><br>
                        Dealer Status
                        <select id="dealer_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" onclick="updateData('no')" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditArea" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Area Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Sub Area ID
                        <input type="text" id="area_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Area Name
                        <input type="text" id="area_areaname" class="form-control" value=""><br>
                        Subarea Name
                        <input type="text" id="area_subareacount" style="background-color: lightgoldenrodyellow" disabled class="form-control" value=""><br>
                        Subarea Status
                        <select id="area_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" onclick="updateData('no')" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditSubarea" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Subareas Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Sub Area ID
                        <input type="text" id="subarea_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Area Name
                        <input type="text" id="subarea_areaname" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Subarea Name
                        <input type="text" id="subarea_subareaname" class="form-control" value=""><br>
                        Salesman Allocated
                        <input type="text" id="subarea_salesman" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Dealers Present
                        <input type="text" id="subarea_dealers" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Subarea Status
                        <select id="subarea_status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" onclick="updateData('no')" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<div id="myModalEditOrder" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Order Details</h4>
            </div>
            <div class="col-md-12">
                <div style="">
                    <div class="modal-body">
                        Order ID
                        <input type="text" name="orderID" id="order_id" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Dealer Name
                        <select name="dealerId" onchange="changeDealer()" id="order_dealername" class="form-control">
                            <?php
                            for($dealerI=0; $dealerI<$dealerCount; $dealerI++)
                            {
                                ?>
                                <option value="<?php echo $dealerIdMain[$dealerI];?>"><?php echo $dealerNameMain[$dealerI]." (".$dealerIdMain[$dealerI].")";?></option>
                            <?php
                            }
                            ?>
                        </select><br>
                        Dealer Contact
                        <input type="text" id="order_dealercontact" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>
                        Dealer Address
                        <input type="text" id="order_dealeraddress" disabled style="background-color: lightgoldenrodyellow" class="form-control" value=""><br>


                        <div>
                            <table class="table table-bordered table-hover table-striped">
                                <thead class="thead-dark">
                                <div>
                                    <tr>
                                        <th style="text-align: center; width: 35%">Item Name</th>
                                        <th style="text-align: center; width: 5%">Quantity</th>
                                        <th style="text-align: center; width: 2%">Operations</th>
                                    </tr>
                                </div>
                                </thead>
                                <tbody>
                                <div>
                                    <?php
                                    for($ordersI=1; $ordersI<=max($totalOrderedItems); $ordersI++)
                                    {
                                        ?>

                                        <tr id="<?php echo 'order_info'.$ordersI; ?>" hidden>
                                            <td style="text-align: center">
                                                <select onchange="computeItemsCost()" name="orderedItems[]" id="<?php echo 'order_itemname'.$ordersI; ?>" class="form-control">
                                                    <?php
                                                    for($itemI=0; $itemI<$productCount; $itemI++)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $productIDMain[$itemI]; ?>"><?php echo $productNameMain[$itemI]." (".$productIDMain[$itemI].")"; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td style="text-align: center"><input value="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" oninput="quantityVerification('<?php echo 'order_quantity'.$ordersI; ?>')" name="itemsQuantity[]" class="form-control" type="number" id="<?php echo 'order_quantity'.$ordersI; ?>"></td>
                                            <td style="text-align: center">
                                                <div style="margin-top: 8px">
                                                    <a onclick="delItem('<?php echo 'order_info'.$ordersI; ?>')">
                                                        <i class="fa-lg fa fa-remove operations" title="Delete"></i>&nbsp;&nbsp;
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </div>


                                <div id="addNewItem">
                                    <?php
                                    for($extraItemsI=0; $extraItemsI<50; $extraItemsI++)
                                    {
                                        ?>
                                        <tr id="<?php echo 'extraItems'.$extraItemsI?>" hidden>
                                            <td style="text-align: center">
                                                <select onchange="computeItemsCost()" name="orderedItems[]" id="<?php echo 'order_extraitemname'.$extraItemsI; ?>" class="form-control">
                                                    <option hidden>Select Item</option>
                                                    <?php
                                                    for($itemI=0; $itemI<$productCount; $itemI++)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $productIDMain[$itemI]; ?>"><?php echo $productNameMain[$itemI]." (".$productIDMain[$itemI].")"; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td style="text-align: center"><input value="1" step="1" onkeypress="return event.charCode >= 48 && event.charCode <= 57" oninput="quantityVerification('<?php echo 'order_extraquantity'.$extraItemsI; ?>')" name="itemsQuantity[]" class="form-control" type="number" id="<?php echo 'order_extraquantity'.$extraItemsI; ?>"></td>
                                            <td style="text-align: center">
                                                <div style="margin-top: 8px">
                                                    <a onclick="delItem('<?php echo 'extraItems'.$extraItemsI; ?>')">
                                                        <i class="fa-lg fa fa-remove operations" title="Delete"></i>&nbsp;&nbsp;
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                </div>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div class="col-md-offset-9">
                                <button class="btn btn-primary" onclick="addNewItem()">Add New Item</button>
                            </div>
                        </div>
                        Order Price
                        <input name="orderPrice" type="number" id="order_orderprice" class="form-control" disabled value=""><br>
                        Bonus
                        <input name="orderBonus" type="text" id="order_bonus" class="form-control" value=""><br>
                        Discount
                        <input name="orderDiscount" step="1" onkeypress="return event.charCode >= 46 && event.charCode <= 57" oninput="updatePrice('discount')" type="number" id="order_discount" class="form-control" value=""><br>
                        Total Price
                        <input name="orderTotalPrice" step="1" onkeypress="return event.charCode >= 46 && event.charCode <= 57" oninput="updatePrice('totalPrice')" type="number" id="order_totalprice" class="form-control" value=""><br>
                        Date
                        <input name="orderDate" type="date" id="order_date" class="form-control" value=""><br>
                        Time
                        <input name="orderTime" type="time" id="order_time" class="form-control" value=""><br>
                        Salesman
                        <select name="salesmanID" id="order_salesman" class="form-control">
                            <?php
                            for($salesmanI=0; $salesmanI<$salesmanCount; $salesmanI++)
                            {
                                ?>
                                <option value="<?php echo $salesmanIDMain[$salesmanI]; ?>"><?php echo $salesmanNameMain[$salesmanI]."<br>(".$salesmanIDMain[$salesmanI].")"; ?></option>
                            <?php
                            }
                            ?>
                        </select><br>
                        Order Status
                        <select name="orderStatus" id="order_status" class="form-control">
                            <option value="Pending">Pending</option>
                            <option value="Ready">Ready</option>
                            <option value="Dispatched">Dispatched</option>
                            <option value="Delivered">Delivered</option>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateData('yes')" data-dismiss="modal">Update</button>
                <button type="button" class="btn btn-default" onclick="updateData('no')" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>
    var extraItem = 0;
    var itemDisplayCount = 0;
    function addNewItem() {
        if(itemDisplayCount<10)
        {
            document.getElementById('extraItems'+extraItem).hidden = false;
            extraItem++;
            itemDisplayCount++;
        }
    }

    function delItem(id) {
        itemDisplayCount--;
        document.getElementById(id).hidden = true;
        computeItemsCost();
    }
    
    function changeDealer() {
        var dealerId = document.getElementById('order_dealername').value;
        var valueIndex = allDealersIds.indexOf(dealerId);
        var dealerNewName = allDealersName[valueIndex];
        var dealerNewContact = allDealersContact[valueIndex];
        var dealerNewAddress = allDealersAddress[valueIndex];
        var dealerNewAreaID = allDealersAreaID[valueIndex];
        document.getElementById('order_dealercontact').value = dealerNewContact;
        document.getElementById('order_dealeraddress').value = dealerNewAddress+" ("+dealerNewAreaID+")";
    }

    function quantityVerification(id) {
        var val = document.getElementById(id).value;
        if(val <= 0)
        {
            document.getElementById(id).value = 1;
        }
        computeItemsCost();
    }
    
    function updatePrice(enteredField) {
        var orderPrice = parseFloat(document.getElementById('order_orderprice').value);
        var discount = parseFloat(document.getElementById('order_discount').value);
        var totalPrice = parseFloat(document.getElementById('order_totalprice').value);


        if(enteredField == "discount")
        {
            if(discount < 0)
            {
                document.getElementById('order_discount').value = 0;
            }
            else
            {
                totalPrice = orderPrice - ((orderPrice/100)*discount);
                document.getElementById('order_totalprice').value = totalPrice.toFixed(2);
            }

        }
        else if(enteredField == 'totalPrice')
        {
            if(totalPrice < 0)
            {
                document.getElementById('order_totalprice').value = 0;
                document.getElementById('order_discount').value = 100;
            }
            else if(totalPrice > orderPrice)
            {
                document.getElementById('order_totalprice').value = orderPrice;
                document.getElementById('order_discount').value = 0;
            }
            else
            {
                discount = 100 - ((totalPrice*100)/orderPrice);
                document.getElementById('order_discount').value = discount.toFixed(2);
            }
        }
        else
        {
            totalPrice = orderPrice - ((orderPrice/100)*discount);
            document.getElementById('order_totalprice').value = totalPrice.toFixed(2);
        }
    }
    
    function computeItemsCost() {
        orderCost = 0;
        for(i1=1; i1<=maxOrdersCount; i1++)
        {
            if(!document.getElementById('order_info'+i1).hidden)
            {
                orderedItem = document.getElementById('order_itemname'+i1).value;
                itemQuantity = parseInt(document.getElementById('order_quantity'+i1).value);
                itemIndex = allProductsIds.indexOf(orderedItem);
                itemCost = allProductsCost[itemIndex]*itemQuantity;
                orderCost = orderCost + itemCost;
            }
        }
        for(i2=0; i2<50; i2++)
        {
            if(!document.getElementById('extraItems'+i2).hidden)
            {
                orderedItem = document.getElementById('order_extraitemname'+i2).value;
                itemQuantity = parseInt(document.getElementById('order_extraquantity'+i2).value);
                itemIndex = allProductsIds.indexOf(orderedItem);
                itemCost = allProductsCost[itemIndex]*itemQuantity;
                orderCost = orderCost + itemCost;
            }
        }
        document.getElementById('order_orderprice').value = parseFloat(orderCost).toFixed(2);
        updatePrice('itemsupdate')
    }

</script>