<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 05-Nov-19
 * Time: 11:44 PM
 */

function array_values_index($num, $arr)
{
    $indexArr = array();
    for($numI=0; $numI<sizeof($arr); $numI++)
    {
        if($arr[$numI] == $num)
        {
            array_push($indexArr, $numI);
        }
    }

    return $indexArr;
}

?>