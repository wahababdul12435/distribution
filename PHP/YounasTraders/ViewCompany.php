<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 27-Oct-19
 * Time: 2:10 PM
 */
require_once "connection.php";
$companiesInfo = "SELECT * FROM `company_info`";
$companiesInfo = mysqli_query($con, $companiesInfo);
$i=0;
while ($data = mysqli_fetch_array($companiesInfo))
{
    $companyID[$i] = $data[0];
    $companyName[$i] = $data[1];
    $companyAddress[$i] = $data[2];
    $companyContact[$i] = $data[3];
    $companyEmail[$i] = $data[4];
    $companyStatus[$i] = $data[5];
    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
        .block-input
        {
            background-color: transparent;
            border-color: transparent;
        }
        .right-left-margin
        {
            margin-left: 2%;
            margin-right: 2%;
        }
    </style>
    <script>
        var companyID = 0;
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>

<div class="container" style="margin-top: 20px">
    <div>
        <table id="CompaniesData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">ID</th>
                    <th style="text-align: center; width: 2%">Name</th>
                    <th style="text-align: center; width: 2%">Address</th>
                    <th style="text-align: center; width: 2%">Contact</th>
                    <th style="text-align: center; width: 2%">Email</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $companyID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyAddress[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyContact[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyEmail[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delCompany('<?php echo $companyID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditCompany" onclick="editData('<?php echo $companyID[$j]; ?>', '<?php echo $companyName[$j]; ?>', '<?php echo $companyAddress[$j]; ?>', '<?php echo $companyContact[$j]; ?>', '<?php echo $companyEmail[$j]; ?>', '<?php echo $companyStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#CompaniesData').DataTable( {
            // scrollX:        true,
            scrollCollapse: true,
            autoWidth:         true,
            paging:         true,
            columnDefs: [
                // { "width": "50px", "targets": [0,1] }
            ]
        } );
    } );

    function delCompany(givenID) {
        companyID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=company_info&op=del&id='+companyID;
        }
        else
        {
            return;
        }
    }

    function editData(id, name, address, contact, email, status) {
        document.getElementById('com_id').value = id;
        document.getElementById('com_name').value = name;
        document.getElementById('com_address').value = address;
        document.getElementById('com_contact').value = contact;
        document.getElementById('com_email').value = email;
        document.getElementById('com_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            com_id = document.getElementById('com_id').value;
            com_name = document.getElementById('com_name').value;
            com_address = document.getElementById('com_address').value;
            com_contact = document.getElementById('com_contact').value;
            com_email = document.getElementById('com_email').value;
            com_status = document.getElementById('com_status').value;

            window.location.href = 'SendData.php?table=company_info&op=update&id='+com_id+'&name='+com_name+'&address='+com_address+'&contact='+com_contact+'&email='+com_email+'&status='+com_status;
        }
        else
        {

        }


    }
</script>
</body>
</html>
