<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 28-Oct-19
 * Time: 12:02 AM
 */

require_once "connection.php";

$getAreasName = "SELECT `subarea_id`, `sub_area_name` FROM `subarea_info`";
$getAreasName = mysqli_query($con, $getAreasName);
$areasCount = 1;
$areaID[0] = 0;
$areaName[0] = "N/A";
while($areaData = mysqli_fetch_array($getAreasName))
{
    $areaID[$areasCount] = $areaData[0];
    $areaName[$areasCount] = $areaData[1];
    $areasCount++;
}

$dealersInfo = "SELECT * FROM `dealer_info`";
$dealersInfo = mysqli_query($con, $dealersInfo);
$i=0;
while ($data = mysqli_fetch_array($dealersInfo))
{
    $dealerID[$i] = $data[0];
    $dealerAreaID[$i] = $data[1];
    $dealerName[$i] = $data[2];
    $dealerContact[$i] = $data[3];
    $dealerAddress[$i] = $data[4];
    $dealerType[$i] = $data[5];
    $dealerCNIC[$i] = $data[6];
    $dealerLicNum[$i] = $data[7];
    $dealerLicExp[$i] = $data[8];
    $dealerStatus[$i] = $data[9];
    $areaIndex = array_search($dealerAreaID[$i], $areaID);
    $dealerAreaName[$i] = $areaName[$areaIndex];
    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
    </style>
    <script>
        var dealerID = 0;
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>
<div class="container">
    <div style="margin-top: 20px">
        <table id="SuppliersData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">ID</th>
                    <th style="text-align: center; width: 2%">Area Name</th>
                    <th style="text-align: center; width: 2%">Name</th>
                    <th style="text-align: center; width: 2%">Contact</th>
                    <th style="text-align: center; width: 2%">Address</th>
                    <th style="text-align: center; width: 2%">Type</th>
                    <th style="text-align: center; width: 2%">CNIC</th>
                    <th style="text-align: center; width: 2%">Lic Num</th>
                    <th style="text-align: center; width: 2%">Lic Exp</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $dealerID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerAreaName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerContact[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerAddress[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerType[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerCNIC[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerLicNum[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerLicExp[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delDealer('<?php echo $dealerID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditDealer" onclick="editData('<?php echo $dealerID[$j]?>', '<?php echo $dealerAreaID[$j];?>', '<?php echo $dealerName[$j]; ?>', '<?php echo $dealerContact[$j];?>', '<?php echo $dealerAddress[$j];?>', '<?php echo $dealerType[$j]; ?>', '<?php echo $dealerCNIC[$j]; ?>', '<?php echo $dealerLicNum[$j]; ?>', '<?php echo $dealerLicExp[$j]; ?>', '<?php echo $dealerStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#SuppliersData').DataTable(
            {
                // "Item No": [[ 0, "desc" ]]
            }
        );
    } );

    function delDealer(givenID) {
        dealerID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=dealer_info&op=del&id='+dealerID;
        }
        else
        {
            return;
        }
    }

    function editData(id, areaname, name, contact, address, type, cnic, licnum, licexp, status) {
        document.getElementById('dealer_id').value = id;
        document.getElementById('dealer_areaname').value = areaname;
        document.getElementById('dealer_name').value = name;
        document.getElementById('dealer_contact').value = contact;
        document.getElementById('dealer_address').value = address;
        document.getElementById('dealer_type').value = type;
        document.getElementById('dealer_cnic').value = cnic;
        document.getElementById('dealer_licnum').value = licnum;
        document.getElementById('dealer_licexp').value = licexp;
        document.getElementById('dealer_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            dealer_id = document.getElementById('dealer_id').value;
            dealer_areaname = document.getElementById('dealer_areaname').value;
            dealer_name = document.getElementById('dealer_name').value;
            dealer_contact = document.getElementById('dealer_contact').value;
            dealer_address = document.getElementById('dealer_address').value;
            dealer_type = document.getElementById('dealer_type').value;
            dealer_cnic = document.getElementById('dealer_cnic').value;
            dealer_licnum = document.getElementById('dealer_licnum').value;
            dealer_licexp = document.getElementById('dealer_licexp').value;
            dealer_status = document.getElementById('dealer_status').value;

            window.location.href = 'SendData.php?table=dealer_info&op=update&id='+dealer_id+'&dealer_areaname='+dealer_areaname+'&dealer_name='+dealer_name+'&dealer_contact='+dealer_contact+'&dealer_address='+dealer_address+'&dealer_type='+dealer_type+'&dealer_cnic='+dealer_cnic+'&dealer_licnum='+dealer_licnum+'&dealer_licexp='+dealer_licexp+'&dealer_status='+dealer_status;
        }
        else
        {

        }
    }
</script>
</body>
</html>