<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 30-Nov-19
 * Time: 11:34 PM
 */
require_once "connection.php";

$subAreaCount = 0;
$getAreaDetails = "SELECT `subarea_id`, `sub_area_name` FROM `subarea_info`";
$getAreaDetails = mysqli_query($con, $getAreaDetails);
while($areaData = mysqli_fetch_array($getAreaDetails))
{
    $subAreaIDMain[$subAreaCount] = $areaData[0];
    $subAreaNameMain[$subAreaCount] = $areaData[1];
    $subAreaCount++;
}

$productCount = 0;
$getProductDetail = "SELECT `product_id`, `product_name`, `final_price` FROM `product_info`";
$getProductDetail = mysqli_query($con, $getProductDetail);
while($productData = mysqli_fetch_array($getProductDetail))
{
    $productIDMain[$productCount] = $productData[0];
    $productNameMain[$productCount] = $productData[1];
    $productPriceMain[$productCount] = $productData[2];
    $productCount++;
}

$dealerCount = 0;
$getDealerInfo = "SELECT `dealer_id`, `dealer_name`, `dealer_area_id`, `dealer_contact`, `dealer_address` FROM `dealer_info`";
$getDealerInfo = mysqli_query($con, $getDealerInfo);
while($dealerData = mysqli_fetch_array($getDealerInfo))
{
    $dealerIdMain[$dealerCount] = $dealerData[0];
    $dealerNameMain[$dealerCount] = $dealerData[1];
    $dealerAreaIdMain[$dealerCount] = $dealerData[2];
    $areaIndex = array_search($dealerAreaIdMain[$dealerCount], $subAreaIDMain);
//    $dealersubAreaName[$dealerCount] = $subAreaNameMain[$areaIndex];
    $dealerContactMain[$dealerCount] = $dealerData[3];
    $dealerAddressMain[$dealerCount] = $dealerData[4];
    $dealerCount++;
}

$salesmanCount = 0;
$getSalesmanInfo = "SELECT `salesman_id`, `salesman_name` FROM `salesman_info`";
$getSalesmanInfo = mysqli_query($con, $getSalesmanInfo);
while($salesmanData = mysqli_fetch_array($getSalesmanInfo))
{
    $salesmanIDMain[$salesmanCount] = $salesmanData[0];
    $salesmanNameMain[$salesmanCount] = $salesmanData[1];
    $salesmanCount++;
}

$orderInfo = "SELECT * FROM `order_info` WHERE `status` = 'Pending'";
$orderInfo = mysqli_query($con, $orderInfo);
$i=0;
while ($data = mysqli_fetch_array($orderInfo))
{
    $orderID[$i] = $data[0];
    $dealerId[$i] = $data[1];
    $dealerIndex = array_search($dealerId[$i], $dealerIdMain);
    $dealerName[$i] = $dealerNameMain[$dealerIndex];
    $dealerAreaID[$i] = $dealerAreaIdMain[$dealerIndex];
//    $dealerSubArea[$i] = $dealersubAreaName[$dealerIndex];
    $dealerContact[$i] = $dealerContactMain[$dealerIndex];
    $dealerAddress[$i] = $dealerAddressMain[$dealerIndex];
    $productId[$i] = $data[2];
    $totalOrderedItems[$i] = 1;
    $productIdsArr = explode("_-_", $productId[$i]);
    if(sizeof($productIdsArr) > 1)
    {
        $totalOrderedItems[$i] = 0;
        $proName = "";
        foreach ($productIdsArr as $proId)
        {
            $totalOrderedItems[$i]++;
            $proIndex = array_search((int)$proId, $productIDMain);
            $proName = $proName."<br>".$productNameMain[$proIndex]." (".$proId.")";
        }
//        $productId[$i] = str_replace("_-_", ",", $productId[$i]);
    }
    else
    {
        $proIndex = array_search($productId[$i], $productIDMain);
        $proName = $productNameMain[$proIndex]." (".$productId[$i].")";
    }
    $productName[$i] = $proName;

    $quantity[$i] = $data[3];
    $quantityArr = explode("_-_", $quantity[$i]);
    if(sizeof($quantityArr) > 1)
    {
        $quant = "";
        foreach ($quantityArr as $quan)
        {
            $quant = $quant."<br>".$quan;
        }
        $quantity[$i] = $quant;
    }

    $unit[$i] = $data[4];
    $unitArr = explode("_-_", $unit[$i]);
    if(sizeof($unitArr) > 1)
    {
        $unitVal = "";
        foreach ($unitArr as $un)
        {
            $unitVal = $unitVal."<br>".$un;
        }
        $unit[$i] = $unitVal;
    }

    $orderPrice[$i] = $data[5];
    $bonus[$i] = $data[6];
    $disccount[$i] = $data[7];
    $totalPrice[$i] = $data[8];
    $date[$i] = $data[9];
    $time[$i] = $data[10];
    $salesmanID[$i] = $data[11];
    $salesmanIndex = array_search($salesmanID[$i], $salesmanIDMain);
    $salesmanName[$i] = $salesmanNameMain[$salesmanIndex];
    $status[$i] = $data[12];
//    echo $orderID[$i]."___".$dealerName[$i]."___".$dealerContact[$i]."___".$dealerAddress[$i]."___".$productName[$i]."___".$quantity[$i]."___".$unit[$i]."___".$date[$i]."___".$time[$i]."___".$salesmanName[$i]."___".$status[$i].'<br>';
    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
        .block-input
        {
            background-color: transparent;
            border-color: transparent;
        }
        .right-left-margin
        {
            margin-left: 2%;
            margin-right: 2%;
        }
    </style>

</head>
<body>
<script type="text/javascript">
    var orderId = 0;
    var maxOrdersCount = <?php echo max($totalOrderedItems); ?>;
    var allDealersIds = <?php echo json_encode($dealerIdMain); ?>;
    var allDealersName = <?php echo json_encode($dealerNameMain); ?>;
    var allDealersContact = <?php echo json_encode($dealerContactMain); ?>;
    var allDealersAddress = <?php echo json_encode($dealerAddressMain); ?>;
    var allDealersAreaID = <?php echo json_encode($dealerAreaIdMain); ?>;

    var allProductsIds = <?php echo json_encode($productIDMain); ?>;
    var allProductsCost = <?php echo json_encode($productPriceMain); ?>;
</script>
<?php
require_once "PopupModel.php";
?>

<div class="container" style="margin-top: 20px">
    <div>
        <table id="ordersData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">ID</th>
                    <th style="text-align: center; width: 2%">Dealer Name</th>
                    <th style="text-align: center; width: 2%">Dealer Contact</th>
                    <th style="text-align: center; width: 2%">Dealer Address</th>
                    <th style="text-align: center; width: 2%">Ordered Items</th>
                    <th style="text-align: center; width: 2%">Quantity</th>
                    <th style="text-align: center; width: 2%">Unit</th>
                    <th style="text-align: center; width: 2%">Total Price</th>
                    <th style="text-align: center; width: 2%">Date</th>
                    <th style="text-align: center; width: 2%">Time</th>
                    <th style="text-align: center; width: 2%">Salesman</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $orderID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerName[$j]." (".$dealerId[$j].")"; ?></td>
                    <td style="text-align: center"><?php echo $dealerContact[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealerAddress[$j]." (".$dealerAreaID[$j].")"; ?></td>
                    <td style="text-align: center"><?php echo $productName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $quantity[$j]; ?></td>
                    <td style="text-align: center"><?php echo $unit[$j]; ?></td>
                    <td style="text-align: center"><?php echo $totalPrice[$j]; ?></td>
                    <td style="text-align: center"><?php echo $date[$j]; ?></td>
                    <td style="text-align: center"><?php echo $time[$j]; ?></td>
                    <td style="text-align: center"><?php echo $salesmanName[$j]."<br>(".$salesmanID[$j].")"; ?></td>
                    <td style="text-align: center"><?php echo $status[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delOrder('<?php echo $orderID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditOrder" onclick="editData('<?php echo $orderID[$j]; ?>', '<?php echo $dealerId[$j]; ?>', '<?php echo $dealerContact[$j]; ?>', '<?php echo $dealerAddress[$j]." (".$dealerAreaID[$j].")"; ?>', '<?php echo $productId[$j]; ?>', '<?php echo $quantity[$j]; ?>', '<?php echo $unit[$j]; ?>', '<?php echo $orderPrice[$j]; ?>', '<?php echo $bonus[$j]; ?>', '<?php echo $disccount[$j]; ?>', '<?php echo $totalPrice[$j]; ?>', '<?php echo $date[$j]; ?>', '<?php echo $time[$j]; ?>', '<?php echo $salesmanID[$j]; ?>', '<?php echo $status[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#ordersData').DataTable( {
            // scrollX:        true,
            scrollCollapse: true,
            autoWidth:         true,
            paging:         true,
            columnDefs: [
                // { "width": "50px", "targets": [0,1] }
            ]
        } );
    } );

    function delOrder(givenID) {
        orderId = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=order_info&op=del&id='+orderId;
        }
        else
        {
            return;
        }
    }

    function editData(id, dealerId, contact, address, orderedItems, quantity, unit, orderPrice, bonus, discount, totalPrice, date, time, salesman, status) {
        date = convertDateFormat(date);
        time = convertTimeFormat(time);
        var itemArr = orderedItems.split("_-_");
        var quantArr = quantity.split("<br>");
        extraItem = 0;
        itemDisplayCount = 0;
        for(x1=1;x1<=maxOrdersCount;x1++)
        {
            document.getElementById('order_info'+x1).hidden = true;
        }
        for(x2=0;x2<50;x2++)
        {
            document.getElementById('extraItems'+x2).hidden = true;
        }
        document.getElementById('order_id').value = id;
        document.getElementById('order_dealername').value = dealerId;
        document.getElementById('order_dealercontact').value = contact;
        document.getElementById('order_dealeraddress').value = address;
        if(itemArr.length>1)
        {
            for(i=1; i<=itemArr.length; i++)
            {
                document.getElementById('order_info'+i).hidden = false;
                document.getElementById('order_itemname'+i).value = itemArr[i-1];
                document.getElementById('order_quantity'+i).value = quantArr[i];
            }
        }
        else
        {
            document.getElementById('order_info1').hidden = false;
            document.getElementById('order_itemname1').value = orderedItems;
            document.getElementById('order_quantity1').value = quantity;
        }

        document.getElementById('order_orderprice').value = orderPrice;
        document.getElementById('order_bonus').value = bonus;
        document.getElementById('order_discount').value = discount;
        document.getElementById('order_totalprice').value = totalPrice;
        document.getElementById('order_date').value = date;
        document.getElementById('order_time').value = time;
        document.getElementById('order_salesman').value = salesman;
        document.getElementById('order_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            order_id = document.getElementById('order_id').value;
            order_dealerId = document.getElementById('order_dealername').value;
            orderedItems = [];
            itemsQuantity = [];
            for(i1=1; i1<=maxOrdersCount; i1++)
            {
                if(!document.getElementById('order_info'+i1).hidden)
                {
                    orderedItems.push(document.getElementById('order_itemname'+i1).value);
                    itemsQuantity.push(document.getElementById('order_quantity'+i1).value);
                }
            }
            for(i2=0; i2<50; i2++)
            {
                if(!document.getElementById('extraItems'+i2).hidden)
                {
                    orderedItems.push(document.getElementById('order_extraitemname'+i2).value);
                    itemsQuantity.push(document.getElementById('order_extraquantity'+i2).value);
                }
            }
            newOrderPrice = document.getElementById('order_orderprice').value;
            newBonus = document.getElementById('order_bonus').value;
            newOrderDiscount = document.getElementById('order_discount').value;
            newOrderTotalPrice = document.getElementById('order_totalprice').value;
            order_date = document.getElementById('order_date').value;
            order_time = document.getElementById('order_time').value;
            order_salesmanId = document.getElementById('order_salesman').value;
            order_status = document.getElementById('order_status').value;

            window.location.href = 'SendData.php?table=order_info&op=update&id='+order_id+'&dealerid='+order_dealerId+'&ordereditems='+orderedItems+'&itemsquantity='+itemsQuantity+'&orderprice='+newOrderPrice+'&bonus='+newBonus+'&discount='+newOrderDiscount+'&totalprice='+newOrderTotalPrice+'&date='+order_date+'&time='+order_time+'&salesmanid='+order_salesmanId+'&status='+order_status;
        }
        else
        {

        }
    }

    function formatDate(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    function convertDateFormat(dateVal) {
        dateArr = dateVal.split("/");
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var mon = months.indexOf(dateArr[1]);
        mon = (mon+1);
        mon = ("0" + mon).slice(-2);
        var newDate = dateArr[2]+"-"+mon+"-"+dateArr[0];
        return newDate;
    }

    function convertTimeFormat(timeVal) {
        var hours = Number(timeVal.match(/^(\d+)/)[1]);
        var minutes = Number(timeVal.match(/:(\d+)/)[1]);
        var AMPM = timeVal.match(/\s(.*)$/)[1];
        if(AMPM == "PM" && hours<12) hours = hours+12;
        if(AMPM == "AM" && hours==12) hours = hours-12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if(hours<10) sHours = "0" + sHours;
        if(minutes<10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }
</script>
</body>
</html>